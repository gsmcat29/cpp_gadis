/**
 * Sum of Two Numbers
 * Write a program that stores the integers 50 and 100 in variables, and stores 
 * the sum of these two in a variable named total.
*/

#include <iostream>
using namespace std;

int main()
{
    int var1 = 50, var2 = 100;
    int total = var1 + var2;

    cout << "The sum of 50 and 100 is: " << total << '\n';

    return 0;
}