/**
 * Circuit Board Price
 * An electronics company sells circuit boards at a 35 percent profit. Write a 
 * program that will calculate the selling price of a circuit board that costs 
 * $14.95. Display the result on the screen.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double PROFIT = 0.35;
    double circuit_price = 14.95;
    double selling_price = 0;

    selling_price = circuit_price + (circuit_price * PROFIT);

    cout << "Final selling price is: $" << selling_price << '\n';

    return 0;
}