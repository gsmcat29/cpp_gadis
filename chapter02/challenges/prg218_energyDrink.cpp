/**
 * Energy Drink Consumption
 * A soft drink company recently surveyed 16,500 of its customers and found that
 * approximately 15 percent of those surveyed purchase one or more energy drinks 
 * per week. Of those customers who purchase energy drinks, approximately 58 
 * percent of them prefer citrus-flavored energy drinks. Write a program that 
 * displays the following:
 * 
 * • The approximate number of customers in the survey who purchase one or more
 * energy drinks per week
 * • The approximate number of customers in the survey who prefer 
 * citrus-flavored energy drinks 
*/


#include <iostream>
using namespace std;

int main()
{
    constexpr double ENERGY_DRINK_WEEKLY = 0.15;
    constexpr double CITRUS_FLAVOR = 0.58;

    int total_customers = 16500;
    double weekly_customers = 0;
    double citrus_customers = 0;

    weekly_customers = ENERGY_DRINK_WEEKLY * total_customers;
    citrus_customers = CITRUS_FLAVOR * weekly_customers;

    cout << "One or more energy drinks per week -> " << weekly_customers << '\n';
    cout << "Citrus-flavored customers -> " << citrus_customers << '\n';

    return 0;
}