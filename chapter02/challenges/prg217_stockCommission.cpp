/**
 * Stock Commission
 * Kathryn bought 750 shares of stock at a price of $35.00 per share. She must 
 * pay her stockbroker a 2 percent commission for the transaction. Write a 
 * program that calculates and displays the following:
 * 
 * • The amount paid for the stock alone (without the commission)
 * • The amount of the commission
 * • The total amount paid (for the stock plus the commission)
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double BROKER_COMMISSION = 0.02;
    double stock_price = 35.00;
    double total_paid = 0.0;
    int number_of_shares = 750;

    double amount_paid = number_of_shares * stock_price;
    double amount_commission = BROKER_COMMISSION * amount_paid;

    total_paid = amount_paid + amount_commission;

    cout << "Amount paid for stock = $" << amount_paid << '\n';
    cout << "Amount of the commission = $" << amount_commission << '\n';
    cout << "Total amount paid = $" << total_paid << '\n';

    return 0;    
}