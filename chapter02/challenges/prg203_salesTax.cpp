;/**
 * Sales Tax
 * Write a program that will compute the total sales tax on a $95 purchase. 
 * Assume the state sales tax is 4 percent and the county sales tax is 2 percent.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double STATE_TAX = 0.04;
    constexpr double COUNTY_TAX = 0.02;

    double purchase = 95;
    double subtotal_state = purchase * STATE_TAX;
    double subtotal_county = purchase * COUNTY_TAX;
    double total_sales = purchase + (subtotal_county + subtotal_state);

    cout << "Total sales = $ " << total_sales << '\n';
}