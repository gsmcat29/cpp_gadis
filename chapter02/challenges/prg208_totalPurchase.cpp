/**
 * Total Purchase
 * A customer in a store is purchasing five items. The prices of the five items 
 * are:
 * 
 * Price of item 1 = $15.95
 * Price of item 2 = $24.95
 * Price of item 3 = $6.95
 * Price of item 4 = $12.95
 * Price of item 5 = $3.95
 * 
 * Write a program that holds the prices of the five items in five variables. 
 * Display each item’s price, the subtotal of the sale, the amount of sales tax,
 *  and the total. Assume the sales tax is 7%.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double SALES_TAX = 0.07;
    double item1, item2, item3, item4, item5;
    double subtotal, amount_sales_tax, total;

    item1 = 15.95;
    item2 = 24.95;
    item3 = 6.95;
    item4 = 12.95;
    item5 = 3.95;

    cout << "Item prices:\n\n$";
    cout << item1 << "\n$" << item2 << "\n$" << item3 << "\n$";
    cout << item4 << "\n$" << item5;
    cout << '\n';

    /*
    You can calculate the sales tax on multiple items by adding together their 
    individual prices and then multiplying the total by the current tax rate.
    */
   subtotal = item1 + item2 + item3 + item4 + item5;
   cout << "Subtotal: $" << subtotal << '\n';

   amount_sales_tax = subtotal * SALES_TAX;
   cout << "Sales Tax: $" << amount_sales_tax << '\n';

   total = subtotal + amount_sales_tax;
   cout << "Total: $" << total << '\n';

   return 0;    
}