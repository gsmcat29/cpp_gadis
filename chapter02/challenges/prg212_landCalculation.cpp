/**
 * Land Calculation
 * One acre of land is equivalent to 43,560 square feet. Write a program that 
 * calculates the number of acres in a tract of land with 391,876 square feet.
*/

#include <iostream>
using namespace std;

int main()
{
    double acre_of_land = 43560;
    double tract_of_land = 391876;

    double number_of_acres = 0;

    number_of_acres = tract_of_land / acre_of_land;

    cout << "The number of acres of land is: " << number_of_acres << '\n';

    return 0;
}