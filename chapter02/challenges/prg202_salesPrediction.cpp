/**
 * Sales Prediction
 * The East Coast sales division of a company generates 58 percent of total 
 * sales. Based on that percentage, write a program that will predict how much 
 * the East Coast division will generate if the company has $8.6 million in 
 * sales this year.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double TOTAL_SALES_PERCENTAGE = 0.58;
    //double sales = 8.6e6;
    double sales = 860000;
    double eastCoastDiv = TOTAL_SALES_PERCENTAGE * sales;
    
    cout << "The EAST COAST DIVISION will generate: $" << eastCoastDiv << '\n';

    return 0;
}