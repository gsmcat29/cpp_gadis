/**
 * Diamond Pattern
 * Write a program that displays the following pattern:
 * 
 * 
            *
           ***
          *****
         *******
          *****
           ***
            *
*/

#include <iostream>
using namespace std;

int main()
{
    cout << "   *   " << '\n';
    cout << "  ***  " << '\n';
    cout << " ***** " << '\n';
    cout << "*******" << '\n';
    cout << " ***** " << '\n';
    cout << "  ***  " << '\n';
    cout << "   *   " << '\n';

    return 0;
}