/**
 * Restaurant Bill
 * Write a program that computes the tax and tip on a restaurant bill for a 
 * patron with a $88.67 meal charge. The tax should be 6.75 percent of the meal 
 * cost. The tip should be 20 percent of the total after adding the tax. 
 * Display the meal cost, tax amount, tip amount, and total bill on the screen.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double TAX = 0.0675;
    constexpr double TIP = 0.20;

    double meal_charge = 88.67;
    double tax_amount = meal_charge * TAX;
    double tip_amount = (meal_charge + tax_amount) * TIP;
    double total_bill = meal_charge + tax_amount + tip_amount;

    cout << "Display Results ***********************************************\n";
    cout << "Meal cost \t\t\t" << meal_charge << '\n';
    cout << "Tax amount\t\t\t" << tax_amount << '\n';
    cout << "Tip amount\t\t\t" << tip_amount << '\n';
    cout << "Total bill\t\t\t" << total_bill << '\n';

}