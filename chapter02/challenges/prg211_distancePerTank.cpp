/**
 * Distance per Tank of Gas
 * A car with a 20-gallon gas tank averages 23.5 miles per gallon when driven in
 *  town and 28.9 miles per gallon when driven on the highway. Write a program 
 * that calculates and displays the distance the car can travel on one tank of 
 * gas when driven in town and when driven on the highway.
 * 
 * Hint: The following formula can be used to calculate the distance:
 * Distance = Number of Gallons * Average Miles per Gallon
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double MPG_TOWN = 23.5;
    constexpr double MPG_HIGHWAY = 28.9;

    int gas_tank = 20;
    double distance_town = gas_tank * MPG_TOWN;
    double distance_highway = gas_tank * MPG_HIGHWAY;

    // display results
    cout << "Distance per tank of gas in town is " << distance_town;
    cout << " miles" <<'\n';
    cout << "Distance per tank of gas in highway is " << distance_highway;
    cout << " miles" << '\n';

    return 0;
}