/**
 * Assuming the ocean’s level is currently rising at about 1.5 millimeters per 
 * year, write a program that displays:
 * 
 * The number of millimeters higher than the current level that the ocean’s 
 * level will be in 5 years
 * 
 * The number of millimeters higher than the current level that the ocean’s 
 * level will be in 7 years
 * 
 * The number of millimeters higher than the current level that the ocean’s 
 * level will be in 10 years
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double CURRENT_RISING = 1.5;
    double level_in_five_years  = 0;
    double level_in_seven_years = 0;
    double level_in_ten_years = 0;

    level_in_five_years = CURRENT_RISING * 5;
    level_in_seven_years = CURRENT_RISING * 7;
    level_in_ten_years = CURRENT_RISING * 10;
    

    cout << "Millimeters higher in 5 years ->" << level_in_five_years << '\n';
    cout << "Millimeters higher in 7 years ->" << level_in_seven_years << '\n';
    cout << "Millimeters higher in 10 years ->" << level_in_ten_years << '\n';

    return 0;
}
