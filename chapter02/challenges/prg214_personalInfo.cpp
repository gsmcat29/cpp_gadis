/**
 * Personal Information
 * Write a program that displays the following pieces of information, each on a 
 * separate line:
 * 
 * Your name
 * Your address, with city, state, and ZIP code
 * Your telephone number
 * Your college major
 * 
 * Use only a single cout statement to display all of this information.
*/

#include <iostream>
using namespace std;

int main()
{
    cout << "John Doe\nViscount BLV, 79925, El Paso Tx.\n123-456-7890 \
    \nComputer Engineering\n";

    return 0;
}