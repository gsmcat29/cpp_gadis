/**
 * Average of Values 
 * To get the average of a series of values, you add the values up and then 
 * divide the sum by the number of values. Write a program that stores the 
 * following values in five different variables: 28, 32, 37, 24, and 33. 
 * The program should first calculate the sum of these five variables and store 
 * the result in a separate variable named sum. Then, the program should divide 
 * the sum variable by 5 to get the average. Display the average on the screen.
*/

#include <iostream>
using namespace std;

int main()
{
    double var0 = 28;
    double var1 = 32;
    double var2 = 37;
    double var3 = 24;
    double var4 = 33;

    double sum = 0;
    double average = 0;

    sum = var0 + var1 + var2 + var3 + var4;
    average = sum / 5;

    cout << "The average value is " << average << '\n';

    return 0;
}