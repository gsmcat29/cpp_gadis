// this program converts seconds to minutes and seconds
#include <iostream>
using namespace std;

int main()
{
    // the total seconds is 125
    int totalSeconds = 125;

    // varaible for the minutes and seconds
    int minutes, seconds;

    // get the remaining minutes
    minutes = totalSeconds / 60;

    // get the reaming seconds
    seconds = totalSeconds % 60;

    // display the results
    cout << totalSeconds << " seconds is equivalent to:\n";
    cout << "Minutes: " << minutes << endl;
    cout << "Seconds: " << seconds << endl;

    return 0;
}