// This program calculates hourly wages, including overtime
#include <iostream>
using namespace std;

int main()
{
    double regularWages,                // to build regular wages
           basePayRate = 18.25,         // base pay rate
           regularHours = 40.0,         // hours worked less overtime
           overtimeWages,               // to hold overtime wages
           overtimePayRate = 27.78,     // overtime pay rate
           overtimeHours = 10,          // overtime hours worked
           totalWages;
    
    // calculate the regular wages
    regularWages = basePayRate * regularHours;

    // calculate the overtime wages
    overtimeWages = overtimePayRate * overtimeHours;

    // calculate the total wages
    totalWages = regularWages + overtimeWages;

    // display the total wages
    cout << "Wages for this week are $" << totalWages << endl;

    return 0;

}