// this program calculates the circumference of a circle
#include <iostream>
using namespace std;

int main()
{
    // constants
    const double PI = 3.14159;
    const double DIAMETER = 10.0;

    // varialbe to hold the circumference
    double circumference;

    // calculate the circumference
    circumference = PI * DIAMETER;

    // display the circumference
    cout << "Thecircumference is " << circumference << endl;

    return 0;
}