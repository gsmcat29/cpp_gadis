//This program writes date to a file
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ofstream outputFile;
    outputFile.open("demofile.txt");

    cout << "Now writing data to the file\n";

    // Write four names to the file
    outputFile << "Bach\n";
    outputFile << "Beethoen\n";
    outputFile << "Mozar\n";
    outputFile << "Schubert\n";

    // close the file
    outputFile.close();

    cout << "Done\n";

    return 0;
}