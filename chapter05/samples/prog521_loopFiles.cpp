// This program reads data from a file
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ofstream outputFile;        // file stream object
    int numberOfDays;           // number of days of sales
    double sales;               // sales amount for a day

    // get the number of days
    cout << "For how many days do you have sales? ";
    cin >> numberOfDays;

    // open a file named sales.txt
    outputFile.open("sales.txt");

    // get the sales for each day and write it to the file
    for (int count = 1; count <= numberOfDays; count++) {
        // get the sales for a day
        cout << "Enter the sales for day " << count << ": ";
        cin >> sales;

        // write the sales to the files
        outputFile << sales << endl;
    }

    // close the file
    outputFile.close();
    cout << "Data writen to sales.txt\n";

    return 0;
}