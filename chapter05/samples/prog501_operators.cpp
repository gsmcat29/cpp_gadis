// This program demonstrates the ++ and -- operators
#include <iostream>
using namespace std;

int main()
{
    int num = 4;    // num starts out with 4

    // display the value in num
    cout << "The variable nums is " << num << endl;
    cout << "I will now increment num\n\n";

    // use postfix ++ to increment num
    num++;
    cout << "Now the variable num is " << num << endl;
    cout << "I will increment num again\n\n";

    // use prefix ++ to increment num
    ++num;
    cout << "Now the variable nums is " << num << endl;
    cout << "I will now decrement num\n\n";

    // use postfix -- to decrement num
    num--;
    cout << "Now the variable nums is " << num << endl;
    cout << "I will decrement num again\n\n";

    // use prefixj -- to decrement
    --num;
    cout << "Now the variable num is " << num << endl;

    return 0;
}