// this program assists a technician in the process of checking a substance's
// temperature
#include <iostream>
using namespace std;

int main()
{
    const double MAX_TEMP = 102.5;      // maximum temperature
    double temperature;                 // to hold the temperature

    // Get the current temperature
    cout << "Enter the subtance's Celsius temperature: ";
    cin >> temperature;

    // As long as necessary, instruct the technician to adjust the thermostat
    while (temperature > MAX_TEMP) {
        cout << "The temperature is too high. Turn the thermostat down and\n";
        cout << "wait 5 minutes. Then take the Celsius temperature again\n";
        cout << "and enter it here: ";
        cin >> temperature;
    }

    // remind the technician to check the temperature again in 15 minutes
    cout << "The temperature is acceptable\n";
    cout << "Check it agian in 15 minutes\n";

    return 0;
}