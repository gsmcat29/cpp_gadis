// This program test for file open errors
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ifstream inputFile;
    int number;

    // open the file
    inputFile.open("BadListOFNumbers.txt");

    // if the file successfully opened, process it
    if (inputFile) {
        // read the numbers from the file and display them
        while (inputFile >> number) {
            cout << number << endl;
        }

        // close the file
        inputFile.close();
    }
    else {
        // display error message
        cout << "Error opening the file\n";
    }

    return 0;
}
