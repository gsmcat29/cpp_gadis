// This program lets the user enter a filename
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

int main()
{
    ifstream inputFile;
    string filename;
    int number;

    // get the filename from the user
    cout << "Enter the filename: ";
    cin >> filename;

    // open the file
    inputFile.open(filename);

    // if the file successfully openeed, process it
    if (inputFile) {
        // read the numbers form the file and display them
        while (inputFile >> number) {
            cout << number << endl;
        }

        // close the file
        inputFile.close();
    }
    else {
        // display an error message
        cout << "Error opening the file\n";
    }

    return 0;
}