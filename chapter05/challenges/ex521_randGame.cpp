/*
Random Number Guessing Game Enhancement
Enhance the program that you wrote for Programming Challenge 20 so it keeps a 
count of the number of guesses that the user makes. When the user correctly 
guesses the random number, the program should display the number of guesses.
*/
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    constexpr int MINIMUM = 1;
    constexpr int MAXIMUM = 10;
    
    int guess_value = 0;
    int user_value = 0;
    int count = 0;

    srand(time(0));

    guess_value = (rand() % (MAXIMUM - MINIMUM + 1)) + MINIMUM;

    //test guess_value
    cout << guess_value << '\n';

    cout << "Try to guess the value: ";

    while (user_value != guess_value) {
        cin >> user_value;

        if (user_value > guess_value) {
            cout << "Too high, try again\n";
        }
        else if (user_value < guess_value) {
            cout << "Too low, try again\n";
        }

        ++count;
    }

    cout << "\nYou guess the value!!!\n";
    cout << "You guess it in " << count << " times\n";

    return 0;
}