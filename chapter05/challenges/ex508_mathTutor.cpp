/*
Math Tutor
This program started in Programming Challenge 15 of Chapter 3, and was modified
in Programming Challenge 9 of Chapter 4. Modify the program again so it displays 
a menu allowing the user to select an addition, subtraction, multiplication, or 
division problem. The final selection on the menu should let the user quit the 
program. After the user has finished the math problem, the program should 
display the menu again.
This process is repeated until the user chooses to quit the program.

Input Validation: If the user selects an item not on the menu, display an error 
message and display the menu again.
*/

#include <iostream>

#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{   
    constexpr int MIN_VALUE = 100;
    constexpr int MAX_VALUE = 999;

    double value1 = 0;
    double value2 = 0;
    double answer = 0;
    double result = 0;

    int menu = 0;

    cout << "Answer the exercise. When you finish type ENTER:\n";
    // Use srand directly with time(0)
    srand(time(0));

    // or use a variable to hold the value
    // unsigned seed = time(0);
    // srand(seed);

    // using rand directly generate big random numbers
    // value1 = rand();
    // value2 = rand();

    // to limit from 100 to 999
    value1 = (rand() % (MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    value2 = (rand() % (MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    answer = value1 + value2;

    cout << "Math tutor menu:\n";
    cout << "1.- Sum\n";
    cout << "2.- Subtraction\n";
    cout << "3.- Multiplication\n";
    cout << "4.- Division\n";
    cout << "5.- QUIT\n";

    cout << "Which Menu option do you want? ";
    cin >> menu;

    while (menu != 5) {
        switch (menu)
        {
        case 1:
            answer = value1 + value2;
            cout << "  " << value1 << '\n';
            cout << "+ " << value2 << '\n';
            cout << "Type answer, then press ENTER: ";
            cin >> result;
            cout << "Calculating ...\n";
            // wait for student to press a key
            cin.get();

            if (result == answer) {
                cout << "CONGRATS. CORRECT ANSWER!!!\n";
            }
            else {
                cout << "INCORRECT\n";
                cout << "Correct answer : " << answer << endl;
            }
            break;
        case 2:
            answer = value1 - value2;
            cout << "  " << value1 << '\n';
            cout << "- " << value2 << '\n';
            cout << "Type answer, then press ENTER: ";
            cin >> result;
            cout << "Calculating ...\n";
            // wait for student to press a key
            cin.get();

            if (result == answer) {
                cout << "CONGRATS. CORRECT ANSWER!!!\n";
            }
            else {
                cout << "INCORRECT\n";
                cout << "Correct answer : " << answer << endl;
            }
            break;
        case 3:
            answer = value1 * value2;
            cout << "  " << value1 << '\n';
            cout << "* " << value2 << '\n';
            cout << "Type answer, then press ENTER: ";
            cin >> result;
            cout << "Calculating ...\n";
            // wait for student to press a key
            cin.get();

            if (result == answer) {
                cout << "CONGRATS. CORRECT ANSWER!!!\n";
            }
            else {
                cout << "INCORRECT\n";
                cout << "Correct answer : " << answer << endl;
            }
            break;                     
        case 4:
            answer = value1 / value2;
            cout << "  " << value1 << '\n';
            cout << "/ " << value2 << '\n';
            cout << "Type answer, then press ENTER: ";
            cin >> result;
            cout << "Calculating ...\n";
            // wait for student to press a key
            cin.get();

            if (result == answer) {
                cout << "CONGRATS. CORRECT ANSWER!!!\n";
            }
            else {
                cout << "INCORRECT\n";
                cout << "Correct answer : " << answer << endl;
            }
            break;        
        default:
            cout << "Program termination\n";
            break;
        }
        
        // show menu again and ask value
        cout << "Math tutor menu:\n";
        cout << "1.- Sum\n";
        cout << "2.- Subtraction\n";
        cout << "3.- Multiplication\n";
        cout << "4.- Division\n";
        cout << "5.- QUIT\n";

        cout << "Which Menu option do you want? ";
        cin >> menu;

    }

    return 0;
}