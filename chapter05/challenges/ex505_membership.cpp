/*
Membership Fees Increase
A country club, which currently charges $2,500 per year for membership, has
announced it will increase its membership fee by 4% each year for the next six 
years.
Write a program that uses a loop to display the projected rates for the next six 
years.
*/

#include <iostream>

int main()
{
    constexpr double YEARLY_MEMBERSHIP = 2500;
    constexpr double FEE_MEMBERSHIP = 0.04;

    double current_rate = YEARLY_MEMBERSHIP;

    
    for (int i = 1; i <= 6; ++i) {
        current_rate += (current_rate * FEE_MEMBERSHIP);
        std::cout << "Year " << i << " = " << current_rate << '\n';
    }

    return 0;
}