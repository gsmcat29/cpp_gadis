/*
Payroll Report
Write a program that displays a weekly payroll report. A loop in the program 
should ask the user for the employee number, gross pay, state tax, federal tax, 
and FICA withholdings. The loop will terminate when 0 is entered for the 
employee number. After the data is entered, the program should display totals 
for gross pay, state tax, federal tax, FICA withholdings, and net pay.

Input Validation: Do not accept negative numbers for any of the items entered. 
Do not accept values for state, federal, or FICA withholdings that are greater 
than the gross pay. If the sum state tax + federal tax + FICA withholdings for 
any employee is greater than gross pay, print an error message and ask the user 
to reenter the data for that employee.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    string employee_number = " ";
    double gross_pay = 0;
    double state_tax = 0;
    double federal_tax = 0;
    double fica = 0;
    double net_pay = 0;

    double fed_discount = 0;
    double state_discount = 0;
    double fica_discount = 0;

    cout << "Enter employee number: ";
    cin >> employee_number;

    cout << "Enter gross pay: ";
    cin >> gross_pay;

    cout << "Enter state tax: ";
    cin >> state_tax;

    if (state_tax < 0 || state_tax > gross_pay) {
        cout << "Invalid values.\nTry again\n";
        exit(1);
    }

    cout << "Enter federal tax: ";
    cin >> federal_tax;

    if (federal_tax < 0 || federal_tax > gross_pay) {
        cout << "Invalid values.\nTry again\n";
        exit(1);
    }

    cout << "Enter fica witholdings: ";
    cin >> fica;
    if (fica < 0 || fica > gross_pay) {
        cout << "Invalid values.\nTry again\n";
        exit(1);
    }    
    // assuming percentages
    fed_discount = gross_pay * (federal_tax / 100.0);
    state_discount = gross_pay * (state_tax / 100.0);
    fica_discount = gross_pay * (fica / 100.0);

    double total_discount = fed_discount + state_discount + fica_discount;
    cout << "total discount " << total_discount << '\n';

    if (total_discount >  gross_pay) {
        cerr << "ERROR, reeenter the data\n";
        exit(1);
    }

    net_pay = gross_pay - total_discount;

    // display results
    cout << "gross pay: " << gross_pay << '\n';
    cout << "state tax: " << state_tax << '\n';
    cout << "fedral tax: " << federal_tax << '\n';
    cout << "fica withold: " << fica << '\n';
    cout << "net pay: " << net_pay << '\n';

    return 0;
}
