/*
Random Number Guessing Game
Write a program that generates a random number and asks the user to guess what 
the number is. If the user’s guess is higher than the random number, the program 
should display “Too high, try again.” If the user’s guess is lower than the 
random number, the program should display “Too low, try again.” The program 
should use a loop that repeats until the user correctly guesses the random 
number.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    constexpr int MINIMUM = 1;
    constexpr int MAXIMUM = 10;
    int guess_value = 0;
    int user_value = 0;

    srand(time(0));

    guess_value = (rand() % (MAXIMUM - MINIMUM + 1)) + MINIMUM;

    //test guess_value
    cout << guess_value << '\n';

    cout << "Try to guess the value: ";

    while (user_value != guess_value) {
        cin >> user_value;

        if (user_value > guess_value) {
            cout << "Too high, try again\n";
        }
        else if (user_value < guess_value) {
            cout << "Too low, try again\n";
        }
    }

    cout << "\nYou guess the value!!!\n";

    return 0;
}