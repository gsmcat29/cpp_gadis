/*
Average Rainfall
Write a program that uses nested loops to collect data and calculate the average 
rainfall over a period of years. The program should first ask for the number of 
years. The outer loop will iterate once for each year. The inner loop will 
iterate twelve times, once for each month. Each iteration of the inner loop will 
ask the user for the inches of rainfall for that month.
After all iterations, the program should display the number of months, the total 
inches of rainfall, and the average rainfall per month for the entire period.

Input Validation: Do not accept a number less than 1 for the number of years. 
Do not accept negative numbers for the monthly rainfall.
*/

#include <iostream>
#include <iomanip>

int main()
{
    int n_years = 0;
    int n_months = 0;
    double rain_inches = 0;
    double total_rain = 0;
    double average_rain = 0;

    std::cout << "Enter number of years: ";
    std::cin >>  n_years;

    if (n_years < 1) {
        std::cerr << "Please type number of years > 1\n";
        std::cerr << "Restart the program\n";
        exit(1);
    }

    for (int i = 1; i <= n_years; ++i) {
        for (int j = 1; j <= 12; ++j) {
            std::cout << "Number of rain inches this month: ";
            std::cin >> rain_inches;

            total_rain += rain_inches;
            ++n_months;
        }
    }

    average_rain = total_rain / n_months;

    // display results
    std::cout << "Number of months: " << n_months << '\n';
    std::cout << "Total inches of rainfall: " << total_rain << '\n';
    std::cout << std::setprecision(2) << std::fixed;
    std::cout << "Average rainfall per month: " << average_rain << '\n';

    return 0;
}