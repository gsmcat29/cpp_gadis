/*
Population Bar Chart
Write a program that produces a bar chart showing the population growth of
Prairieville, a small town in the Midwest, at 20-year intervals during the past 
100 years.
The program should read in the population figures (rounded to the nearest 1,000 
peole) for 1900, 1920, 1940, 1960, 1980, and 2000 from a file. For each year it 
should display the date and a bar consisting of one asterisk for each 1,000 
people. The data can be found in the People.txt file.

Here is an example of how the chart might begin:

PRAIRIEVILLE POPULATION GROWTH
(each * represents 1,000 people)
1900 **
1920 ****
1940 *****
*/

#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    constexpr int STAR = 1000;

    int value = 0;
    int n_star = 0;
    int year = 1900;

    ifstream inFile;
    string n_people;

    inFile.open("people.txt");
    cout << "Reading data from file\n";

    while (inFile >> value) {
        //cout << value << endl;
        n_star = value / STAR;
        cout <<  year  << ": "  << string(n_star, '*') << '\n';
        year += 20;
    }


    inFile.close();

    return 0;
}