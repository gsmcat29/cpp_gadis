/*
The distance a vehicle travels can be calculated as follows:
distance = speed * time
For example, if a train travels 40 miles per hour for 3 hours, the distance 
traveled is 120 miles.
Write a program that asks the user for the speed of a vehicle (in miles per hour) 
and how many hours it has traveled. The program should then use a loop to 
display the distance the vehicle has traveled for each hour of that time period. 
Here is an example of the output:

What is the speed of the vehicle in mph? 40
How many hours has it traveled? 3
Hour Distance Traveled
--------------------------------
1                 40
2                 80
3                120

Input Validation: Do not accept a negative number for speed and do not accept 
any
value less than 1 for time traveled.
*/

#include <iostream>
#include <iomanip>

int main()
{
    double speed = 0;
    double hours = 0;
    double distance = 0;

    std::cout << "What is the speed of the vehicle in mph? ";
    std::cin >> speed;

    if (speed < 0) {
        std::cerr << "ERROR, values less than 0 are not valid\n";
        std::cerr << "PLEASE RESTART THE PROGRAM\n";
    }

    std::cout << "How many hhours has it traveled? ";
    std::cin >> hours;

    // input validation
    if (hours < 1) {
        std::cerr << "ERROR, values less than 1 are not valid\n";
        std::cerr << "PLEASE RESTART THE PROGRAM\n";
    }

    std::cout << "Hour Distance Traveled\n";
    std::cout << "-------------------------------------\n";
    
    for (int i = 1; i <= hours; ++i) {
        distance = speed * i;
        std::cout << i << "\t\t" << std::setw(6)  << distance << std::endl;
    }

}