/*
Using Files—Student Line Up
Modify the Student Line Up program described in Programming Challenge 14 so that
it gets the names from a file. Names should be read in until there is no more 
data to read. If you have downloaded this book’s source code from the companion 
Web site, you will find a file named LineUp.txt in the Chapter 05 folder. You 
can use this file to test the program.
*/

#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    string name = " ";
    string last_student = " ";
    string first_student = " ";


    ifstream inFile;
    inFile.open("lineup.txt");

    inFile >> name;
    first_student = last_student = name; // equal the string to then compare

    while (inFile >> name) {
        

        if (name < first_student) {
            first_student = name;
        }
        else if (name > last_student) {
                last_student = name;
        }


    }


    cout << "First student: " << first_student << endl;
    cout << "Last student: " << last_student << endl;

    inFile.close();

    return 0;
}