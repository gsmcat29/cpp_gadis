/*
Student Line Up
A teacher has asked all her students to line up single file according to their 
first name.
For example, in one class Amy will be at the front of the line and Yolanda will 
be at the end. Write a program that prompts the user to enter the number of 
students in the class, then loops to read that many names. Once all the names 
have been read it reports which student would be at the front of the line and 
which one would be at the end of the line. You may assume that no two students 
have the same name.

Input Validation: Do not accept a number less than 1 or greater than 25 for the 
number of students.
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
    int n_students = 0;
    string name = " ";
    string last_student = " ";
    string first_student = " ";

    cout << "Enter the number of students: ";
    cin >> n_students;

    if (n_students < 1 || n_students > 25) {
        cerr << "Error, range of students 1 ~ 25\nTry again\n";
        exit(1);
    }

    cout << "Enter name of student: ";
    cin >> name;

    first_student = last_student = name; // equal the string to then compare

    if (name < first_student) {
        first_student = name;
    }
    else if (name > last_student) {
            last_student = name;
    }

    for (int i = 1; i < n_students; ++i) {
        cout << "Enter name  of student: ";
        cin >> name;

        if (name < first_student) {
            first_student = name;
        }
        else if (name > last_student) {
            last_student = name;
        }
    }

    cout << "First student: " << first_student << endl;
    cout << "Last student: " << last_student << endl;

    return 0;
}