/*
Using Files—Savings Account Balance Modification
Modify the Savings Account Balance program described in Programming Challenge 16
so that it writes the final report to a file.
*/

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

int main()
{
    ofstream outFile;
    outFile.open("balance.txt");

    double annual_rate = 0.0;
    double balance = 0.0;
    double deposit = 0.0;
    double withdraw = 0.0;
    double monthly_interest = 0.0;
    
    int n_months = 0;
    double total_deposits = 0;
    double total_withdraw = 0;
    double total_interest = 0;
    
    cout << "Enter starting balance: ";
    cin >> balance;

    cout << "Enter number of months with account: ";
    cin >> n_months;

    cout << "Enter annual interest rate: ";
    cin >> annual_rate;

    monthly_interest = annual_rate / 12.0;


    for (int i = 0; i < n_months; ++i) {
        cout << "Deposit amount this month: ";
        cin >> deposit;
        balance += deposit;
        total_deposits += deposit;

        cout << "Withdraw amount this month: ";
        cin >> withdraw;
        balance -= withdraw;
        total_withdraw += withdraw;

        balance += balance * (monthly_interest/100.0);
        total_interest += balance * (monthly_interest/100.0);
    }

    //display results
    outFile << "\n*************************************\n";
    outFile << setprecision(2) << fixed;
    outFile << "Ending balance: " << balance << '\n';
    outFile << "Total amount of deposits: " << total_deposits << '\n';
    outFile << "Total amount of withdraws: " << total_withdraw << '\n';
    outFile << "Total interest: " << total_interest << '\n';

    outFile.close();

    return 0;
}