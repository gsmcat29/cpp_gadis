/*
Write a program that will predict the size of a population of organisms. 
The program should ask the user for the starting number of organisms, their 
average daily population increase (as a percentage), and the number of days they 
will multiply. A loop should display the size of the population for each day.

Input Validation: Do not accept a number less than 2 for the starting size of 
the population. Do not accept a negative number for average daily population 
increase. Do not accept a number less than 1 for the number of days they will 
multiply.
*/

#include <iostream>
#include <iomanip>

int main()
{
    double n_organims = 0;
    double avg_population_increase = 0;
    double n_days = 0;
    double total_population = 0;

    std::cout << "Enter number of organisms: ";
    std::cin >> n_organims;

    if (n_organims < 2) {
        std::cerr << "Enter a quantity greater than 2\n";
        std::cerr << "Restart the program!!\n";
        exit(1);
    }
    
    std::cout << "Enter average daily population increase (%): ";
    std::cin >> avg_population_increase;

    if (avg_population_increase < 0) {
        std::cerr << "I cannot accept negative numbers. Restart the program\n";
        exit(1);
    }
    
    std::cout << "Enter number of days: ";
    std::cin >> n_days;

    if (n_days < 1) {
        std::cerr << "I cannot accept numbers less than 1. Restart the program\n";
        exit(1);
    }

    for (int i = 1; i <= n_days; ++i) {
        total_population += n_organims * (avg_population_increase/100.0);
    }

    std::cout << "The total population after " << n_days << " is ";
    std::cout << std::setprecision(2) << std::fixed << total_population;

    std::cout << '\n';

    return 0;
}
