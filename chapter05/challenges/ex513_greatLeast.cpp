/*
The Greatest and Least of These
Write a program with a loop that lets the user enter a series of integers. The 
user should enter −99 to signal the end of the series. After all the numbers 
have been entered, the program should display the largest and smallest numbers 
entered.
*/

#include <iostream>


int main()
{
    int value = 0;
    int largest = 1;
    int smallest = 1;

    while (value != -99) {
        std::cout << "Enter a value (-99 to exit): ";
        std::cin >> value;

        if (value > largest) {
            largest = value;
        }
        else if (value < smallest && value != -99) {
            smallest = value;
        }
        
    }

    std::cout << "The largest value is " << largest << '\n';
    std::cout << "The smallest value is:" << smallest << '\n';

    return 0;
}