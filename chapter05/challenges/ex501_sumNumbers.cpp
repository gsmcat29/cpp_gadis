/*
Sum of Numbers
Write a program that asks the user for a positive integer value. The program 
should use a loop to get the sum of all the integers from 1 up to the number 
entered. For example, if the user enters 50, the loop will find the sum of 
1, 2, 3, 4, … 50.

Input Validation: Do not accept a negative starting number.
*/

#include <iostream>
using namespace std;

int main()
{
    int value = 0;
    int sum = 0;

    cout << "Enter a positive integer value: ";
    cin >> value;

    if (value < 0) {
        cerr << "Error!!!\n";
        cout << "Restart the program using positive integer\n";
    }

    for (int i = 1; i <= value; ++i) {
        sum += i;
    }

    cout << "Total sum = " << sum << endl;

    return 0;
}