/*
Sales Bar Chart
Write a program that asks the user to enter today’s sales for five stores. 
The program should then display a bar graph comparing each store’s sales. Create 
each bar in the bar graph by displaying a row of asterisks. Each asterisk should 
represent $100 of sales.

Here is an example of the program’s output.

Enter today's sales for store 1: 1000 [Enter]
Enter today's sales for store 2: 1200 [Enter]
Enter today's sales for store 3: 1800 [Enter]
Enter today's sales for store 4: 800 [Enter]
Enter today's sales for store 5: 1900 [Enter]

SALES BAR CHART
(Each * = $100)
Store 1: **********
Store 2: ************
Store 3: ******************
Store 4: ********
Store 5: *******************

*/

#include <iostream>
using namespace std;

int main()
{
    double sales_0 = 0;
    double sales_1 = 0;
    double sales_2 = 0;
    double sales_3 = 0;
    double sales_4 = 0;

    int star = 0;
    char ch = '*';

    cout << "Enter today's sales for store 1: ";
    cin >> sales_0;
    cout << "Enter today's sales for store 2: ";
    cin >> sales_1;
    cout << "Enter today's sales for store 3: ";
    cin >> sales_2;
    cout << "Enter today's sales for store 4: ";
    cin >> sales_3;
    cout << "Enter today's sales for store 5: ";
    cin >> sales_4;


    cout << "\n\nSales Bar chart:\n";
    cout << "(Each * = 100)\n";

    for (int i = 0; i < 5; ++i) {
        if (i == 0 ) {
            star = sales_0 / 100;
            cout << "Store " << i << ": "  << string(star, '*') << '\n';
        }
        else if (i == 1) {
            star = sales_1 / 100;
            cout << "Store " << i << ": " << string(star, '*') << '\n';
        }
        else if (i == 2) {
            star = sales_2 / 100;
            cout << "Store " << i << ": "<< string(star, '*') << '\n';
        }
        else if (i == 3) {
            star = sales_3 / 100;
            cout << "Store " << i << ": " << string(star,'*') << '\n';
        }
        else if (i == 4) {
            star = sales_4 / 100;
            cout << "Store " << i << ": " << string(star, '*') << '\n';
        }
    }

    cout << '\n';

    return 0;
}