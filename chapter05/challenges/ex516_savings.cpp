/*
Savings Account Balance
Write a program that calculates the balance of a savings account at the end of a 
period of time. It should ask the user for the annual interest rate, the 
starting balance, and the number of months that have passed since the account 
was established. A loop should then iterate once for every month, performing the 
following:

A) Ask the user for the amount deposited into the account during the month. (Do 
not accept negative numbers.) This amount should be added to the balance.

B) Ask the user for the amount withdrawn from the account during the month. (Do
not accept negative numbers.) This amount should be subtracted from the balance.

C) Calculate the monthly interest. The monthly interest rate is the annual 
interest rate divided by twelve. Multiply the monthly interest rate by the 
balance, and add the result to the balance.

After the last iteration, the program should display the ending balance, the 
total amount of deposits, the total amount of withdrawals, and the total 
interest earned.

NOTE: If a negative balance is calculated at any point, a message should be 
displayed indicating the account has been closed and the loop should terminate.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double annual_rate = 0.0;
    double balance = 0.0;
    double deposit = 0.0;
    double withdraw = 0.0;
    double monthly_interest = 0.0;
    
    int n_months = 0;
    double total_deposits = 0;
    double total_withdraw = 0;
    double total_interest = 0;
    
    cout << "Enter starting balance: ";
    cin >> balance;

    cout << "Enter number of months with account: ";
    cin >> n_months;

    cout << "Enter annual interest rate: ";
    cin >> annual_rate;

    monthly_interest = annual_rate / 12.0;


    for (int i = 0; i < n_months; ++i) {
        cout << "Deposit amount this month: ";
        cin >> deposit;
        balance += deposit;
        total_deposits += deposit;

        cout << "Withdraw amount this month: ";
        cin >> withdraw;
        balance -= withdraw;
        total_withdraw += withdraw;

        balance += balance * (monthly_interest/100.0);
        total_interest += balance * (monthly_interest/100.0);
    }

    //display results
    cout << "\n*************************************\n";
    cout << setprecision(2) << fixed;
    cout << "Ending balance: " << balance << '\n';
    cout << "Total amount of deposits: " << total_deposits << '\n';
    cout << "Total amount of withdraws: " << total_withdraw << '\n';
    cout << "Total interest: " << total_interest << '\n';

    return 0;
}