/*
Characters for the ASCII Codes
Write a program that uses a loop to display the characters for the ASCII codes 0
through 127. Display 16 characters on each line.
*/

#include <iostream>

int main()
{
    char ch = ' ';
    int counter = 0;

    for (int i = 0; i < 128; ++i) {
        ch = i;
        std::cout << ch << '\t';
        ++counter;

        if (counter == 16) {
            std::cout << '\n';
            counter = 0;
        }
    }

    std::cout << std::endl;

    return 0;
}