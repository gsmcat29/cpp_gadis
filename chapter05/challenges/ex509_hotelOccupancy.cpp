/*
Hotel Occupancy
Write a program that calculates the occupancy rate for a hotel. The program 
should start by asking the user how many floors the hotel has. A loop should 
then iterate once for each floor. In each iteration, the loop should ask the 
user for the number of rooms on the floor and how many of them are occupied. 
After all the iterations, the program should display how many rooms the hotel 
has, how many of them are occupied, how many are unoccupied, and the percentage 
of rooms that are occupied. The percentage may be calculated by dividing the 
number of rooms occupied by the number of rooms.

N OTE: It is traditional that most hotels do not have a thirteenth floor. 
The loop in this program should skip the entire thirteenth iteration.

Input Validation: Do not accept a value less than 1 for the number of floors. 
Do not accept a number less than 10 for the number of rooms on a floor.
*/

#include <iostream>
#include <iomanip>

int main()
{
    int n_floors = 0;
    int n_rooms = 0;
    int occupied_rooms = 0;

    int total_rooms = 0;
    int total_occupued = 0;

    int total_unoccupied = 0;
    double room_percentage = 0;

    std::cout << "How many floors in the hotel? ";
    std::cin >> n_floors;

    if (n_floors < 1) {
        std::cerr << "ERROR!! PLEASE RESTART THE PROGRAM!!\n";
        std::cerr << "TYPE VALUES GREATER THAN 1 FOR NUMBER OF FLOORS\n";
        exit(1);
    }

    for (int i = 1; i <= n_floors; ++i) {

        if (i == 13) {
            continue;;
        }

        std::cout << "Number of rooms in this floor? ";
        std::cin >> n_rooms;

        if (n_rooms < 10) {
            std::cerr << "Error. You should use 10 or more floors\n";
            std::cerr << "Restart the program\n";
            exit(1);
        }

        total_rooms += n_rooms;

        std::cout << "How many are occupied? ";
        std::cin >> occupied_rooms;
        total_occupued += occupied_rooms;
    }


    // compute results
    std::cout << "Total rooms:      " << total_rooms << '\n';
    std::cout << "Occupied rooms:   " << total_occupued << '\n';

    total_unoccupied = total_rooms - total_occupued;   
    std::cout << "Unoccupied rooms: " <<  total_unoccupied << '\n';

    room_percentage =  static_cast<double> (total_occupued) / total_rooms;
    std::cout << std::setprecision(2) << std::fixed;
    std::cout << "Room percentage:  " << room_percentage << std::endl;

    return 0;
}