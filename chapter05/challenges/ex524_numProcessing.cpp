/*
Using Files—Numeric Processing
If you have downloaded this book’s source code from the companion Web site, you
will find a file named Random.txt in the Chapter 05 folder. This file contains a 
long list of random numbers. Copy the file to your hard drive and then write a 
program that opens the file, reads all the numbers from the file, and calculates 
the following:

A) The number of numbers in the file
B) The sum of all the numbers in the file (a running total)
C) The average of all the numbers in the file

The program should display the number of numbers found in the file, the sum of 
the numbers, and the average of the numbers.
*/

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream inFile;

    int n_values = 0;
    int count = 0;
    int sum = 0;
    double average = 0;

    // open file
    inFile.open("random.txt");
    cout << "Reading data from file\n";

    while (inFile >> n_values) {
        ++count;
        sum += n_values;
    }

    cout << "How many numbers in file? " << count << endl;
    cout << "The sum of all the numbers: " << sum << endl;

    average = static_cast<double> (sum) / count;

    cout << "The average of all numbers: " << average << endl;

    // close file
    inFile.close();

    return 0;
}