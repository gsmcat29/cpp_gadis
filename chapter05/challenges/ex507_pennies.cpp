/*
Pennies for Pay
Write a program that calculates how much a person would earn over a period of 
time if his or her salary is one penny the first day and two pennies the second 
day, and continues to double each day. The program should ask the user for the 
number of days.
Display a table showing how much the salary was for each day, and then show the
total pay at the end of the period. The output should be displayed in a dollar 
amount, not the number of pennies.
Input Validation: Do not accept a number less than 1 for the number of days 
worked.
*/

#include <iostream>

int main()
{
    int number_days = 0;
    int pennies = 1;
    double dollars = 0;

    std::cout << "Please enter number of days worked: ";
    std::cin >> number_days;

    if (number_days < 1) {
        std::cerr << "PLEASE ENTER A VALUE GREATER THAN 1\n";
        std::cerr << "RESTART THE PROGRAM!!\n";
    }

    for (int i = 1; i < number_days; ++i) {
        pennies *= 2;
    }

    std::cout << "pennies = " << pennies << std::endl;

    dollars = pennies / 100.0;
    std::cout << "dollars = " << dollars << std::endl;

    return 0;
}