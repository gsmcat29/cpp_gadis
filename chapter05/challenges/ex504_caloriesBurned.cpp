/*
Calories Burned
Running on a particular treadmill you burn 3.6 calories per minute. Write a 
program that uses a loop to display the number of calories burned after 5, 10, 
15, 20, 25, and 30 minutes.
*/

#include <iostream>

int main()
{
    constexpr double CALORIES_BURN_MINUTE = 3.6;
    double calories = 0;

    for (int i = 5; i <= 30; i+=5 ) {
        calories = CALORIES_BURN_MINUTE * i;
        std::cout << "Burned calories in " << i << " minutes = " << calories;
        std::cout << std::endl;
    }

    std::cout << std::endl;

    return 0;
}
