/*
Celsius to Fahrenheit Table
In Programming Challenge 10 of Chapter 3 you were asked to write a program that
converts a Celsius temperature to Fahrenheit. Modify that program so it uses a 
loop to display a table of the Celsius temperatures 0–20, and their Fahrenheit 
equivalents.
*/

#include <iostream>
#include <iomanip>

int main()
{
    double celsius = 0;
    double fahrenheit = 0;

    std::cout << "Table of temperature from 0 to 20\n";
    
    std::cout << std::setprecision(2) << std::fixed;

    for (int i = 1; i <= 20; ++i) {
        celsius = i;
        fahrenheit = (9 * celsius) / 5 + 32.0;
        std::cout << i << " *C\t\t" << fahrenheit << " *F\n";
    }

    //std::cout << std::setprecision(2) << std::fixed;

    return 0;    
}