/*
Ocean Levels
Assuming the ocean’s level is currently rising at about 1.5 millimeters per year, 
write a program that displays a table showing the number of millimeters that the 
ocean will ave risen each year for the next 25 years.
*/

#include <iostream>
#include <iomanip>

int main()
{
    constexpr double CURRENT_LEVEL = 1.5;   // millimeters
    constexpr int YEARS = 25;
    
    double total_lelvel = 0;

    for (int i = 1; i <= YEARS; ++i) {
        total_lelvel += CURRENT_LEVEL;
    }

    std::cout << "The total level will be " << total_lelvel << " millimeters\n";

    return 0;
}