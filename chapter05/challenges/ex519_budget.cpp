/*
Budget Analysis 
Write a program that asks the user to enter the amount that he or she has 
budgeted for a month. A loop should then prompt the user to enter each of his
 or her expenses for the month and keep a running total. When the loop 
 finishes, the program should display the amount that the user is over or 
 under budget.
*/

#include <iostream>
using namespace std;

int main()
{
    double month_budget  = 0.0;
    double total_expenses = 0.0;
    double expenses = 0.0;

    cout << "Enter initial budget: ";
    cin >> month_budget;
    cout << "Enter expenses (press q to quit): ";

    while (cin >> expenses) {
        month_budget -= expenses;
        //total_expenses += expenses;
        cout << "Enter expenses (press q to quit): ";
    }

    //cout << "Total expenses: " << total_expenses << '\n';
    
    if (month_budget < 0) {
        cout << "You are " << -1 * month_budget << " over\n";
    }
    else {
        cout << "You are " << month_budget << " under\n";
    }

    return 0;
}