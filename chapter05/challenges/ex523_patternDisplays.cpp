/*
Pattern Displays
Write a program that uses a loop to display Pattern A below, followed by another
 loop that displays Pattern B

Pattern A                   Pattern B
+                           ++++++++++
++                          +++++++++
+++                         ++++++++
++++                        +++++++
+++++                       ++++++
++++++                      +++++
+++++++                     ++++
++++++++                    +++
+++++++++                   ++
++++++++++                  +
*/

#include <iostream>
#include <string>

using namespace std;

int main()
{
    string patternb = "++++++++++";
    char patterna = '+';

    // print pattern a
    printf("Pattern A\n");
    for (int i = 0; i < 10; ++i) {
        printf("%c", patterna);
        for (int j = 0; j < i; ++j) {
            printf("%c", patterna);
        }

        printf("\n");
    }

    // print patternb
    printf("\nPattern B\n");

    for (int k = 0; k < 10; ++k) {
        cout << patternb << '\n';
        patternb.pop_back();
    }

    return 0;
}