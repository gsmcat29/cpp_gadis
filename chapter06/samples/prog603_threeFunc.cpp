// This program has three functions: main, first, and second
#include <iostream>
using namespace std;

// *******************************
// Definition of function fisrst
// This function displays a message
//*********************************
void first()
{
    cout << "I am now inside the function first\n";
}

/* Definiton of function second
This function displays a message*/
void second()
{
    cout << "I am now inside the function second\n";
}

/* Function main */

int main()
{
    cout << "I am starting in function main\n";
    first();        // call function first
    second();       // call function second
    cout << "Back in functiiom main agian\n";

    return 0;
}