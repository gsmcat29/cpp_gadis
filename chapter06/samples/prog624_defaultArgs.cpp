// this program demonstrates default function arguments
#include <iostream>
using namespace std;

// function prototype with default arguments
void displayStars(int = 10, int = 1);

int main()
{
    displayStars();     // use default value for cols and rows
    cout << endl;
    displayStars(5);    // use defaultvalue for rows
    cout << endl;
    displayStars(7, 3); // use 7 for colts, and 3 for rows

    return 0;
}

/**
 * Definiton of function displayStars
 * The default argument for cols is 10 and for rows is 1
 * This funciton displays a square made of asterisks
*/

void displayStars(int cols, int rows)
{
    // nested loop. Outer loop controls the rows and the inner loop controls the
    // columns
    for (int down = 0; down < rows; down++)
    {
        for (int across = 0; across < cols; across++) {
            cout << "*";
        }
        cout << endl;
    }
}
