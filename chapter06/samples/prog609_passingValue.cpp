// This program demonstrates that changes to a function parameter have no 
// effect on the original argument

#include <iostream>
using namespace std;

// function prototype
void changeMe(int);

int main()
{
    int number = 12;

    // Display the value in number
    cout << "number is " << number << endl;

    // chall changeMe, passing the value in number as an argument
    changeMe(number);

    // display the value in number again
    cout << "Now back in main again, the value of number is " << number << endl;

    return 0;
}

/**
 * Definition of function changeMe
 * This function changes the value fo the parameter myValue
*/

void changeMe(int myValue)
{
    // change the vlaue of myValue to 0
    myValue = 0;
    
    // display the value in myValue
    cout << "Now the value is " << myValue << endl;
}