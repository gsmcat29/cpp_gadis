// This program converts cups to fluid ounces
#include <iostream>
#include <iomanip>
using namespace std;

// function prototypes
void showIntro();
double getCups();
double cupsToOunces(double);

int main()
{
    // variables for the cups and ounces
    double cups, ounces;

    // set up numeric output formatting
    cout << fixed << showpoint << setprecision(1);


    // display an intro to screen
    showIntro();

    // get the number of cups
    cups = getCups();

    // convert cups to fliud ounces
    ounces = cupsToOunces(cups);

    // display the number of ounces
    cout << cups << " cups equals " << ounces << " ounces\n";

    return 0;
}


/**
 * The showIntro function displays an introuctory screen
*/

void showIntro()
{
    cout << "This program converts measurements in cups to fliud ounces\n"
         << "For your reference the formula is\n"
         << " 1 cup = 8 fluid ounces\n\n";
}


double getCups()
{
    double numCups;

    cout << "Enter the number of cups: ";
    cin >> numCups;
    return numCups;
}


/**
 * The cupsToOunces function accepts a number of cups as an argument and returns
 * the equivalent number of fliud ounces as a double
*/

double cupsToOunces(double numCups)
{
    return numCups * 8.0;
}

