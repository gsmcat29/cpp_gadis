// this program has an unitialized global variable
#include <iostream>
using namespace std;

int globalNum;      // global variable, automatically set to zero

int main()
{
    cout << "globalNum is " << globalNum << endl;

    return 0;
}