// This program shows that a static local variable is only initialized once
#include <iostream>
using namespace std;

void showStatic();      // function prototype

int main()
{
    // call the showStatic function fives times
    for (int count = 0; count < 5; count++) {
        showStatic();
    }

    return 0;
}

/**
 * Definiton of function showStatic
 * stateNum is a static local varaible. Its value is displayed and then 
 * incremented just before the function returns
*/

void showStatic()
{
    static int statNum = 5;

    cout << "statNum is " << statNum << endl;
    statNum++;
}