// this program uses a reference variable as a function parameter
#include <iostream>
using namespace std;

// function prototype. The parameter is a reference variable
void doubleNum(int &);
void getNum(int &);

int main()
{
    int value;

    // get a number and store it in value
    getNum(value);

    // double the number stored in value
    doubleNum(value);

    // display the resulting number
    cout << "The value double is is " << value << endl;

    return 0;
}


/**
 * Definiton of getNum
 * The parameter userNum is a reference variable. The user is asked to enter a
 * number, which is stored in userNum
*/

void getNum(int &userNum)
{
    cout << "Enter a number: ";
    cin >> userNum;
}


/**
 * Definition of doubleNum
 * The parameter refVar is a reference variable. The value in refVar is double
*/

void doubleNum(int &refVar)
{
    refVar *= 2;
}