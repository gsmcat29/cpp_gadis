/**
 * This program demonstrates two value-returning functions
 * The square function is called in a mathematical statement
*/

#include <iostream>
#include <iomanip>
using namespace std;

// funciton prototypes
double getRadius();
double square(double);

int main()
{
    const double PI = 3.14159;      // constant for pi
    double radius;                  // to hold the circle's area
    double area;                    // to hold the circle's area

    // set the numeric output formatting
    cout << fixed << showpoint << setprecision(2);

    // get the radius of the circle
    cout << "This program calculates the area of a circle\n";
    radius = getRadius();

    // calculate the area of the circle
    area = PI * square(radius);

    // display the area
    cout << "The area is " << area << endl;

    return 0;
}


/**
 * Definition of function getRadius
 * This function ask the user to enter the radius of the circle and then 
 * returns that number as a double
*/

double getRadius()
{
    double rad;

    cout << "Enter the radius of the circle: ";
    cin >> rad;

    return rad;
}


/**
 * Definition of function square
 * This function accepts a double argument and returns the square of the
 * argument as a double
*/

double square(double number)
{
    return number * number;
}