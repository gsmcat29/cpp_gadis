// This program demonstrates a function with a parameter
#include <iostream>
using namespace std;

// function prototype
void displayValue(int);

int main()
{
    cout << "I am passing 5 to displayValue\n";
    displayValue(5);    // Call displayValue with argument 5
    displayValue(10);   // call displayValue with argument 10
    displayValue(2);   // call displayValue with argument 2
    displayValue(16);   // call displayValue with argument 16

    cout << "Now I am back in main\n";

    return 0;
}

/**
 * Definition of function displayValue
 * It uses an integer parameter whose value is displayed
*/

void displayValue(int num)
{
    cout << "The value is " << num << endl;
}