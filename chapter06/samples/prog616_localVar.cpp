// This program shows that variables defined in a function are hidden from 
// other functions
#include <iostream>
using namespace std;

void anotherFunction();     // function prototype

int main()
{
    int num = 1;            // local variable

    cout << "In main, num is " << num << endl;
    anotherFunction();
    cout << "Back in main, nm is " << num << endl;

    return 0;
}


/**
 * Definition of anotherFunciton
 * It has a local variable, num, whse initial value is displayed
*/

void anotherFunction()
{
    int num = 20;       // local variable
    cout << "In anotheFunciton, num is " << num << endl;
}