// this program uses a function that returns true or false
#include <iostream>
using namespace std;

// function prototype
bool isEven(int);

int main()
{
    int val;

    // get a number from the user
    cout << "Enter an integer and I will tell you if it is even or odd: ";
    cin >> val;

    // Indicate whether it is even or odd
    if (isEven(val))
        cout << val << " is even\n";
    else
        cout << val << " is odd\n";

    return 0;
}

/**
 * Definition of function isEven
 * This function accepts an integer argument and test it to be even or odd
 * The function returns true if the argument i even or false if the argument is
 * odd. The return value is odd
*/

bool isEven(int number)
{
    bool status;

    if (number % 2 == 0)
        status = true;      // the number is even if there is no remainder
    else
        status = false;
    
    return status;
}