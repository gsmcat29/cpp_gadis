// this program shows how the exit function causes a program to stop executing
#include <iostream>
//#include <cstdlib>      // need for exit function
using namespace std;

void function();        // function prototype

int main()
{
    function();

    return 0;
}

/**
 * This funciton simply demonstrates that exit can be used to terminate a program
 * from a function other than main
*/

void function()
{
    cout << "This program terminates with the exit function\nBye!\n";
    exit(0);
    cout << "This message will never be displayed\n";
    cout << "because the program has already terminated\n";
}
