/*
Spectral Analysis
If a scientist knows the wavelength of an electromagnetic wave, he or she can 
determine what type of radiation it is. Write a program that asks for the 
wavelength of an electromagnetic wave in meters and then displays what that wave 
is according to the char below (see book page 224)
*/

#include <iostream>
using namespace std;

int main()
{
    double wavelength = 0;

    cout << "Enter wavelenght (sientific notation): ";
    cin >> wavelength;

    cout << "Computing result . . .\n";
    
    if (wavelength > 1E-2) {
        cout << "Radio Waves\n";
    }
    else if (wavelength > 1E-3 && wavelength <= 1E-2) {
        cout << "Microwaves\n";
    }
    else if (wavelength < 7E-7 && wavelength <= 1E-3) {
        cout << "Infrared\n";
    }
    else if (wavelength < 4E-7 && wavelength <= 7E-7) {
        cout << "Visible Light\n";
    }
    else if (wavelength < 1E-8 && wavelength <= 4E-7) {
        cout << "Ultraviolet\n";
    }
    else if (wavelength < 1E-11 && wavelength <= 1E-8) {
        cout << "Xrays\n";
    }
    else if (wavelength < 1E-11) {
        cout << "Gamma Rays\n";
    }

    return 0;
}