/*
Software Sales
A software company sells a package that retails for $99. Quantity discounts are 
given according to the following table.


Quantity                        Discount
10–19                           20%
20–49                           30%
50–99                           40%
100 or more                     50%

Write a program that asks for the number of units sold and computes the total 
cost of the purchase.
Input Validation: Make sure the number of units is greater than 0.
*/
#include <iostream>
using namespace std;

int main()
{
    constexpr double DISCOUNT_A = 0.2;
    constexpr double DISCOUNT_B = 0.3;
    constexpr double DISCOUNT_C = 0.4;
    constexpr double DISCOUNT_D = 0.5;
    constexpr double PACKAGE = 99;

    int number_units = 0;
    double total_cost = 0;

    cout << "Enter the amount of units sold: ";
    cin >> number_units;

    if (number_units < 0) {
        cerr << "Error!\n";
        return 1;
    }
    else if (number_units >= 1 && number_units < 10) {
        total_cost = PACKAGE * number_units;
    }
    else if (number_units >= 10 && number_units < 20) {
        total_cost = (PACKAGE * number_units);
        total_cost = total_cost - (total_cost * DISCOUNT_A);
    }
    else if (number_units >= 20 && number_units < 50) {
        total_cost = (PACKAGE * number_units);
        total_cost = total_cost - (total_cost * DISCOUNT_B);
    }
    else if (number_units >= 50 && number_units < 100) {
        total_cost = (PACKAGE * number_units);
        total_cost = total_cost - (total_cost * DISCOUNT_C);        
    }
    else if (number_units >= 100) {
        total_cost = (PACKAGE * number_units);
        total_cost = total_cost - (total_cost * DISCOUNT_D);        
    }

    cout << "The total cost is: " << total_cost << endl;

    return 0;
}