/*
Minimum/Maximum
Write a program that asks the user to enter two numbers. The program should use 
the conditional operator to determine which number is the smaller and which is 
the larger.
*/

#include <iostream>
using namespace std;

int main()
{
    int value1 = 0;
    int value2 = 0;

    cout << "Enter two values separated by space: ";
    cin >> value1 >> value2;

    if (value1 > value2) {
        cout << "Larger value:  " << value1 << endl;
        cout << "Smaller Value: " << value2 << endl;
    }
    else {
        cout << "Larger value:  " << value2 << endl;
        cout << "Smaller value: " << value1 << endl;
    }

    cout << '\n';

    return 0;
}