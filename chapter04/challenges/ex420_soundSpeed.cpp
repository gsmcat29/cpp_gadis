/*
The Speed of Sound
The following table shows the approximate speed of sound in air, water, 
and steel.

Medium          Speed
Air             1,100 feet per second
Water           4,900 feet per second
Steel           16,400 feet per second

Write a program that displays a menu allowing the user to select air, water, or 
steel. After the user has made a selection, he or she should be asked to enter 
the distance a sound wave will travel in the selected medium. The program will 
then display the amount of time it will take. (Round the answer to four decimal 
places.)
Input Validation: Check that the user has selected one of the available choices 
from the menu. Do not accept distances less than 0.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double AIR = 1100;
    constexpr double WATER = 4900;
    constexpr double STEEL = 16400;

    int medium = 0;
    double distance = 0;
    double time_sound = 0;
    // v = d/t -> t = d/v

    cout << "Menu:\n";
    cout << "Select the number of the option:\n";
    cout << "1) AIR\n2) WATER\n3) STEEL\n\n";

    
    cout << "Enter numetic value of the medium: ";
    cin >> medium;

    cout << "Enter distance in feet: ";
    cin >> distance;

    cout << fixed << setprecision(4);

    if (medium == 1) {
        cout << "You choose Air\n";
        time_sound = distance / AIR;
        cout << time_sound << " seconds\n";
    }
    else if (medium == 2) {
        cout << "You choose Water\n";
        time_sound = distance / WATER;
        cout << time_sound << " seconds\n";        
    }
    else if (medium == 3) {
        cout << "You choose Steel\n";
        time_sound = distance / STEEL;
        cout << time_sound << " seconds\n";        
    }
    else {
        cout << "Invalid input\nRestart the program\n";
        exit(1);
    }

    return 0;
}