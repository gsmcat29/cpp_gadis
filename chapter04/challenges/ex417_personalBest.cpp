/*
Personal Best
Write a program that asks for the name of a pole vaulter and the dates and vault
heights (in meters) of the athlete’s three best vaults. It should then report, 
in order of height (best first), the date on which each vault was made and its 
height.
Input Validation: Only accept values between 2.0 and 5.0 for the heights.
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{   
    constexpr double MIN_HEIGHT = 2.0;
    constexpr double MAX_HEIGHT = 5.0;

    string athelte_name = "";
    string date_a, date_b, date_c;
    double height_a, height_b, height_c;    // meters

    cout << "Enter pole vaulter name: ";
    getline(cin , athelte_name);
    cout << "Enter height 1 (min = 2.0, max= 5.0): ";
    cin >> height_a;
    cin.get();
    cout << "Enter date of height 1: ";
    getline(cin, date_a);

    cout << "Enter height 2 (min = 2.0, max= 5.0): ";
    cin >> height_b;
    cin.get();
    cout << "Enter date of height 2: ";
    getline(cin, date_b);

    cout << "Enter height 3 (min = 2.0, max= 5.0): ";
    cin >> height_c;
    cin.get();
    cout << "Enter date of height 3: ";
    getline(cin, date_c);

    cout << "\n\n\n";
    cout << "Order of heights in progress ...\n";

    if (height_a >= height_b && height_a >= height_c) {
        cout << "1) " << height_a << " -- " << date_a << '\n';
        if (height_b > height_c) {
            cout << "2) " << height_b << " -- " << date_b << '\n';
            cout << "3) " << height_c << " -- " << date_c << '\n';
        }
        else {
            cout << "2) " << height_c << " -- " << date_c << '\n';
            cout << "3) " << height_b << " -- " << date_b << '\n';            
        }
    }
    else if (height_b >= height_a && height_b >= height_c) {
         cout << "1) " << height_b << " -- " << date_b << '\n';
        if (height_a > height_c) {
            cout << "2) " << height_a << " -- " << date_a << '\n';
            cout << "3) " << height_c << " -- " << date_c << '\n';
        }
        else {
            cout << "2) " << height_c << " -- " << date_c << '\n';
            cout << "3) " << height_a << " -- " << date_a << '\n';            
        }       
    }
    else if (height_c >= height_a && height_c >= height_b) {
         cout << "1) " << height_c << " -- " << date_c << '\n';
        if (height_a > height_b) {
            cout << "2) " << height_a << " -- " << date_a << '\n';
            cout << "3) " << height_b << " -- " << date_b << '\n';
        }
        else {
            cout << "2) " << height_b << " -- " << date_b << '\n';
            cout << "3) " << height_a << " -- " << date_a << '\n';            
        } 
    }


    return 0;
}