/*
Mobile Service Provider, Part 2
Modify the Program in Programming Challenge 25 so that it also displays how much
money Package A customers would save if they purchased packages B or C, and how
much money Package B customers would save if they purchased Package C. If there 
would be no savings, no message should be printed.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double PKG_A_PRICE = 39.99;
    constexpr double PKG_B_PRICE = 59.99;
    constexpr double PKG_C_PRICE = 69.99;

    constexpr int PKG_A_MINS = 450;
    constexpr int PKG_B_MINS = 900;

    constexpr double PKG_A_EXTRA = 0.45;
    constexpr double PKG_B_EXTRA = 0.40;

    char choice = ' ';
    double number_minutes = 0;
    double additional_minutes = 0;
    double total_cost = 0;

    double savings = 0;

    cout << "Which package are you using: ";
    cin >> choice;

    cout << "How many minutes were used?:  ";
    cin >> number_minutes;

    cout << "Calculating ...\n\n";

    if (choice == 'A' || choice == 'a') {
        if (number_minutes > PKG_A_MINS) {
            additional_minutes = PKG_A_EXTRA * (number_minutes - PKG_A_MINS);
            total_cost = additional_minutes + PKG_A_PRICE;
        }
        else {
            total_cost = PKG_A_PRICE;
        }
    }
    else if (choice == 'B' || choice == 'b') {
        if (number_minutes > PKG_B_MINS) {
            additional_minutes = PKG_B_EXTRA * (number_minutes - PKG_B_MINS);
            total_cost = additional_minutes + PKG_B_PRICE;
        }
        else {
            total_cost = PKG_B_PRICE;
        }
    }
    else if (choice == 'C' || choice == 'c') {
        total_cost = PKG_C_PRICE;
    }
    else {
        cerr << "Incorrect input. Try AGAIN!!!\n";
        exit(1);
    }

    cout << fixed << setprecision(2);
    cout << "Total cost: $" << total_cost << endl;

    // compute savings ========================================================
    if ((choice == 'A'  && total_cost > PKG_C_PRICE) || (choice == 'A'  && total_cost > PKG_B_PRICE)) {
        savings = total_cost - PKG_B_PRICE;
        cout << "Buying package B you will save: $" << savings << endl;
        savings = total_cost - PKG_C_PRICE;
        cout << "Buying package C you will save: $" << savings << endl;
    }
    else if (choice == 'B' && total_cost > PKG_C_PRICE) {
        savings = total_cost - PKG_C_PRICE;
        cout << "Buying package C you will save: $" << savings << endl;        
    }

    return 0;
}