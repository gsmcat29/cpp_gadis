/*
Bank Charges
A bank charges $10 per month plus the following check fees for a commercial 
checking account:

$.10 each for fewer than 20 checks
$.08 each for 20–39 checks
$.06 each for 40–59 checks
$.04 each for 60 or more checks

The bank also charges an extra $15 if the balance of the account falls below 
$400 (before any check fees are applied). Write a program that asks for the 
beginning balance and the number of checks written. Compute and display the 
bank’s service fees for the month.
Input Validation: Do not accept a negative value for the number of checks 
written. If a negative value is given for the beginning balance, display an 
urgent message indicating the account is overdrawn.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr int MONTHLY_CHARGE =  10;
    constexpr int BELOW_400 = 15;
    constexpr double  CHECKS_FEE0 = 0.10;
    constexpr double  CHECKS_FEE1 = 0.08;
    constexpr double  CHECKS_FEE2 = 0.06;
    constexpr double  CHECKS_FEE3 = 0.04;

    double balance = 0;
    double total_fees = 0;
    int number_checks = 0;

    cout << "Enter balance amount: ";
    cin >> balance;
    cout << "Enter number of checks: ";
    cin >> number_checks;

    if (balance < 400) {
        total_fees += BELOW_400;
    }

    if (number_checks < 0) {
        cout << "ACCOUNT IS OVERDRAW!!!\n";
    }
    else if (number_checks >= 1 && number_checks < 20) {
        total_fees += MONTHLY_CHARGE + (CHECKS_FEE0 * number_checks);
    }
    else if (number_checks >= 20 && number_checks < 40) {
        total_fees += MONTHLY_CHARGE + (CHECKS_FEE1 * number_checks);
    }
    else if (number_checks >= 40 && number_checks < 59) {
        total_fees += MONTHLY_CHARGE + (CHECKS_FEE2 * number_checks);
    }
    else if (number_checks >= 60) {
        total_fees += MONTHLY_CHARGE + (CHECKS_FEE3 * number_checks);
    }

    cout << "Total service fees for the month: " << total_fees << endl;

    return 0;
}