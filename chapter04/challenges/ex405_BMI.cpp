/*
Body Mass Index
Write a program that calculates and displays a person’s body mass index (BMI). 
The BMI is often used to determine whether a person with a sedentary lifestyle 
is overweight or underweight for his or her height. A person’s BMI is calculated 
with the following formula:

BMI = weight X 703 / height^2

where weight is measured in pounds and height is measured in inches. The program
should display a message indicating whether the person has optimal weight, is 
underweight, or is overweight. A sedentary person’s weight is considered to be 
optimal if his or her BMI is between 18.5 and 25. If the BMI is less than 18.5, 
the person is considered to be underweight. If the BMI value is greater than 25, 
the person is considered to be overweight.
*/

#include <iostream>
using namespace std;

int main()
{
    double weight = 0;
    double height = 0;
    double bmi = 0;
    constexpr int IMPERIAL_CONSTANT = 703;

    cout << "Enter your height in inches: ";
    cin >> height;

    cout << "Enter your weight in pounds: ";
    cin >> weight;

    bmi = (IMPERIAL_CONSTANT * weight) / (height * height);

    if (bmi > 25) {
        cout << "You are overweight\n";
    }
    else if (bmi > 18.5 && bmi < 25) {
        cout << "You are on optimal weight\n";
    }
    else if (bmi < 18.5) {
        cout << "You are underweight\n";
    }

    return 0;
}