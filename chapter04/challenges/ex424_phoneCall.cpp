/*
Long-Distance Calls
A long-distance carrier charges the following rates for telephone calls:

Starting Time of Call               Rate per Minute
00:00–06:59                         0.05
07:00–19:00                         0.45
19:01–23:59                         0.20

Write a program that asks for the starting time and the number of minutes of the 
call, and displays the charges. The program should ask for the time to be 
entered as a floating-point number in the form HH.MM. For example, 07:00 hours 
will be entered as 07.00, and 16:28 hours will be entered as 16.28.

Input Validation: The program should not accept times that are greater than 
23:59. Also, no number whose last two digits are greater than 59 should be 
accepted. 

Hint: Assuming num is a floating-point variable, the following expression will 
give you its fractional part:

num − static_cast<int>(num)

*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double RATE_MINUTE_A = 0.05;
    constexpr double RATE_MINUTE_B = 0.45;
    constexpr double RATE_MINUTE_C = 0.20;

    double number_minutes = 0;
    double start_time = 0;
    double last_digits = 0;
    double call_cost = 0;

    cout << "Enter call start time (HH.MM): ";
    cin >> start_time;

    last_digits = start_time - static_cast<int>(start_time);

    if (start_time > 23.59 || last_digits > 59) {
        cerr << "INPUT ERROR!! RESTART THE PROGRAM\n";
        exit(1);
    }

    cout << "Enter number of minutes of the call: ";
    cin >> number_minutes;

    /*if (extra_time > LIMIT_A || extra_time > LIMIT_B || extra_time > LIMIT_C) {
        remainder_minutes = last_digits;
    }*/

    if (start_time >= 0.0 && start_time < 6.59) {
        call_cost += number_minutes * RATE_MINUTE_A;
    }
    else if (start_time >= 7.0 && start_time < 19.0) {
        call_cost += number_minutes * RATE_MINUTE_B;
    }
    else if (start_time >= 19.01 && start_time < 23.59) {
        call_cost += number_minutes * RATE_MINUTE_C;
    }

    cout << "Total Cost of call : " << call_cost << endl;

    return 0;
}
