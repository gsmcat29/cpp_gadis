/*
The Speed of Sound in Gases
When sound travels through a gas, its speed depends primarily on the density of 
the medium. The less dense the medium, the faster the speed will be. 
The following table shows the approximate speed of sound at 0 degrees centigrade, 
measured in meters per second, when traveling through carbon dioxide, air, 
helium, and hydrogen.

MediumSpeed                         (Meters per Second)
Carbon Dioxide                      258.0
Air                                 331.5
Helium                              972.0
Hydrogen                            1,270.0

Write a program that displays a menu allowing the user to select one of these 
four gases. After a selection has been made, the user should enter the number of 
seconds it took for the sound to travel in this medium from its source to the 
location at which it was detected. The program should then report how far away 
(in meters) the source of the sound was from the detection location.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double CO2_SPEED = 258.0;
    constexpr double AIR_SPEED = 331.5;
    constexpr double HEL_SPEED = 972.0;
    constexpr double HYD_SPEED = 1270.0;

    int gas = 0;
    double seconds = 0;
    double distance = 0;
    // v = d/t -> d = vt
    cout << "Enter a gas value:\n\n";
    cout << "1) CO2\t2)AIR\t3)Helium\t4)Hydrogen\n";
    cin >> gas;

    cout << "Enter number of seconds: ";
    cin >> seconds;

    if (seconds < 0 || seconds > 30) {
        cout << "Incorrent input of time\n";
        exit(1);
    }

    switch (gas)
    {
    case 1:
        distance = CO2_SPEED * seconds;
        cout << "The total distance is " << distance << endl;
        break;
    case 2:
        distance = AIR_SPEED * seconds;
        cout << "The total distance is " << distance << endl;
        break;
    case 3:
        distance = HEL_SPEED * seconds;
        cout << "The total distance is " << distance << endl;
        break;
    case 4:
        distance = HYD_SPEED * seconds;
        cout << "The total distance is " << distance << endl;
        break;
    default:
        cout << "Incorrect input\n";
        exit(1);
        //break;
    }

    return 0;
}