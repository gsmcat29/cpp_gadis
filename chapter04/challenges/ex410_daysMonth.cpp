/*
Write a program that asks the user to enter the month (letting the user enter an 
integer in the range of 1 through 12) and the year. The program should then 
display the number of days in that month. Use the following criteria to identify 
leap years:

1. Determine whether the year is divisible by 100. If it is, then it is a leap year if and
only if it is divisible by 400. For example, 2000 is a leap year but 2100 is not.
2. If the year is not divisible by 100, then it is a leap year if and if only it is divisible by
4. 

For example, 2008 is a leap year but 2009 is not.

Here is a sample run of the program:
Enter a month (1-12): 2 [Enter]
Enter a year: 2008 [Enter]
29 days
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
    constexpr int DAYS_JAN = 31;
    constexpr int DAYS_FEB_LEAP = 29;
    constexpr int DAYS_FEB = 28;
    constexpr int DAYS_MAR = 31;
    constexpr int DAYS_APR = 30;
    constexpr int DAYS_MAY = 30;
    constexpr int DAYS_JUN = 30;
    constexpr int DAYS_JUL = 31;
    constexpr int DAYS_AGO = 31;
    constexpr int DAYS_SEP = 30;
    constexpr int DAYS_OCT = 31;
    constexpr int DAYS_NOV = 30;
    constexpr int DAYS_DEC = 31;

    int month = 0;
    int year = 0;
    bool is_leap;

    cout << "Enter a month (1-12): ";
    cin >> month;
    cout << "Enter a year: ";
    cin >> year;

    // calculate if year is leap or not
    if ((year % 100 != 0 && year % 4 == 0) || year % 400 == 0) {
        is_leap = true;
    }
    else {
        is_leap = false;
    }

    //cout << boolalpha;
    //cout << "Leap: " << is_leap << endl;

    switch (month)
    {
    case 1:
        cout << DAYS_JAN << " days\n";
        break;
    case 2:
        if (is_leap) {
            cout << DAYS_FEB_LEAP << " days\n";
        }
        else {
            cout << DAYS_FEB << " days\n";
        }
        break;
    case 3:
        cout << DAYS_MAR << " days\n";
        break;
    case 4:
        cout << DAYS_APR << " days\n";          
        break;
    case 5:
        cout << DAYS_MAY << " days\n";
        break;
    case 6: 
        cout << DAYS_JUN << " days\n";
        break;
    case 7:
        cout << DAYS_JUL << " days\n";
        break;
    case 8:
        cout << DAYS_AGO << " days\n";
        break;
    case 9:
        cout << DAYS_SEP << " days\n";
    case 10:
        cout << DAYS_OCT << " days\n";
        break;
    case 11:
        cout << DAYS_NOV << " days\n" ;
        break;
    case 12:
        cout << DAYS_DEC << " days\n";
        break;
    default:
        cout << "Incorrect inout. Try again\n";
        break;
    }

    return 0;
}