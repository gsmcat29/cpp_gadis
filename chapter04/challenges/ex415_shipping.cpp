/*
Shipping Charges
The Fast Freight Shipping Company charges the following rates:

Weight of Package (in Kilograms)                    Rate per 500 Miles Shipped
2 kg or less                                        $1.10
Over 2 kg but not more than 6 kg                    $2.20
Over 6 kg but not more than 10 kg                   $3.70
Over 10 kg but not more than 20 kg                  $4.80

Write a program that asks for the weight of the package and the distance it is 
to be shipped, and then displays the charges.
Input Validation: Do not accept values of 0 or less for the weight of the 
package. Do not accept weights of more than 20 kg (this is the maximum weight 
the company will ship). Do not accept distances of less than 10 miles or more 
than 3,000 miles. These are the company’s minimum and maximum shipping distances.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr int MAX_DISTANCE = 3000;
    constexpr int MIN_DISTANCE =   10;
    constexpr int MAX_WEIGHT = 20;
    constexpr int MIN_WEIGHT =  0;

    // Rate per 500 miles
    constexpr double RATE_0 = 1.10;
    constexpr double RATE_1 = 2.20;
    constexpr double RATE_2 = 3.70;
    constexpr double RATE_3 = 4.80;

    double package_weight = 0;
    double shipping_distance = 0;
    double total_charges = 0;
    double distance_multiplier = 0;

    cout << "Enter package weight: ";
    cin >> package_weight;
    cout << "Enter shipping distance: ";
    cin >> shipping_distance;

    // input validation first
    if (shipping_distance < MIN_DISTANCE || shipping_distance > MAX_DISTANCE) {
        cout << "The company cannot accept the shipping distance\n";
        cout << "Please try again\n";
        return 1;
    }
    else if (package_weight < MIN_WEIGHT || package_weight > MAX_WEIGHT) {
        cout << "The company cannot accept the package weight\n";
        cout << "Please try again\n";
        return 1;
    }


    distance_multiplier = shipping_distance / 500;
    // logic
    if (package_weight > MIN_WEIGHT && package_weight <= 2) {
        total_charges = distance_multiplier * RATE_0;
    }
    else if (package_weight > 2 && package_weight <= 6) {
        total_charges = distance_multiplier * RATE_1;
    }
    else if (package_weight > 6 && package_weight <= 10) {
        total_charges = distance_multiplier * RATE_2;
    }
    else if (package_weight > 10 && package_weight <= MAX_WEIGHT) {
        total_charges = distance_multiplier * RATE_3;
    }

    cout << "Total charges = " << total_charges << endl;

    return 0;
}