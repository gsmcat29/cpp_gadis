/*
Magic Dates
The date June 10, 1960 is special because when we write it in the following 
format, the month times the day equals the year.

6/10/60

Write a program that asks the user to enter a month (in numeric form), a day, 
and a two-digit year. The program should then determine whether the month times 
the day is equal to the year. If so, it should display a message saying the date 
is magic. Otherwise it should display a message saying the date is not magic.
*/


#include <iostream>
using namespace std;


int main()
{
    int day = 0;
    int month = 0;
    int year = 0;
    int magic_date = 0;

    cout << "Enter a month, day, two-digit year:\t";
    cin >> month >> day >> year;

    magic_date = month * day;

    if (magic_date == year) {
        cout << month << "/" << day << "/" << year << " is magic date\n";
    }
    else {
        cout << month << "/" << day << "/" << year << " is not a magic date\n";
    }

    return 0;
}