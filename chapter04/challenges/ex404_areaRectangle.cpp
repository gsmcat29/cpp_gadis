/*
Areas of Rectangles
The area of a rectangle is the rectangle’s length times its width. Write a 
program that asks for the length and width of two rectangles. The program should 
tell the user which rectangle has the greater area, or if the areas are the same.
*/

#include <iostream>
using namespace std;

int main()
{
    double length0 = 0, length1 = 0;
    double width0 = 0, width1;
    double area0 = 0, area1 = 0;

    cout << "Enter length and width of rectangle 0: ";
    cin >> length0 >> width0;

    cout << "Enter length and width of rectangle 1: ";
    cin >> length1 >> width1;

    area0 = length0 * width0;
    area1 = length1 * width1;

    if (area0 > area1) {
        cout << "Rectangle 0 has greater area than Rectangle 1\n";
    }
    else if (area1 > area0) {
        cout << "Rectangle 1 has a greater area than Rectangle 0\n";
    }
    else if (area0 == area1) {
        cout << "Both rectangles have the same area\n";
    }

    return 0;
}