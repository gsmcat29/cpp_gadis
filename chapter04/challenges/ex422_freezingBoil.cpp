/*
Freezing and Boiling Points
The following table lists the freezing and boiling points of several substances. 
Write a program that asks the user to enter a temperature and then shows all the 
substances that will freeze at that temperature and all that will boil at that 
temperature. For example, if the user enters −20 the program should report that 
water will freeze and oxygen will boil at that temperature.

Substance               Freezing Point (°F)                   Boiling Point (°F)
Ethyl alcohol           -173                                  172
Mercury                 -38                                   676
Oxygen                  -362                                 -306
Water                   32                                   212

*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr int FREEZING_ETHYL = -173;
    constexpr int FREEZING_MERCR = -38;
    constexpr int FREEZING_OXYGN = -362;
    constexpr int FREEZING_WATER = 32;

    constexpr int BOILING_ETHYL = 172;
    constexpr int BOILING_MERCR = 676;
    constexpr int BOILING_OXYGN = -306;
    constexpr int BOILING_WATER = 212;

    double temperature = 0;

    cout << "Enter a temperature (Fahrenheit): ";
    cin >> temperature;

    // freezing point
    if (temperature < FREEZING_WATER) {
        cout << "Water will freeze\n";
    }
    else if (temperature <= FREEZING_MERCR && temperature > FREEZING_ETHYL) {
        cout << "Mercury will freeze\n";
    }
    else if (temperature <= FREEZING_ETHYL && temperature > FREEZING_OXYGN) {
        cout << "Mercury will freeze\n";
        cout << "Ethyl will freeze\n";
    }
    else if (temperature <= FREEZING_OXYGN) {
        cout << "Mercury will freeze\n";
        cout << "Ethyl will freeze\n";
        cout << "Oxygen will freeze\n";
    }
    else {
        cout << "Nothing will freeze\n";
    }

    cout << endl;
    // boilin gpoint
    if (temperature > BOILING_OXYGN) {
        cout << "Oxygen will boil\n";
    }
    else if (temperature >= BOILING_ETHYL && temperature < BOILING_WATER) {
        cout << "Oxygen will boil\n";
        cout << "Ethyl will boil\n";
    }
    else if (temperature >= BOILING_WATER && temperature < BOILING_MERCR) {
        cout << "Oxygen will boil\n";
        cout << "Ethyl will boil\n";        
        cout << "Water will boil\n";
    }
    else if (temperature >= BOILING_MERCR) {
        cout << "Oxygen will boil\n";
        cout << "Ethyl will boil\n";        
        cout << "Water will boil\n";
        cout << "Mercury will boil\n";
    }
    else {
        cout << "Nothing will boil\n";
    }

    return 0;
}