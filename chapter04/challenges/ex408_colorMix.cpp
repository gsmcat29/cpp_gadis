/*
The colors red, blue, and yellow are known as the primary colors because they 
cannot be made by mixing other colors. When you mix two primary colors, you get 
a secondary color, as shown here:

When you mix red and blue, you get purple.
When you mix red and yellow, you get orange.
When you mix blue and yellow, you get green.

Write a program that prompts the user to enter the names of two primary colors 
to mix. If the user enters anything other than “red,” “blue,” or “yellow,” 
the program should display an error message. Otherwise, the program should 
display the name of the secondary color that results by mixing two primary 
colors.
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
    string color0 = "";
    string color1 = "";
    string new_color = "";

    cout << "Enter two color name:\t";
    cin >> color0 >> color1;

    if ((color0 == "red" && color1 == "blue")
    || (color0 == "blue" && color1 == "orange")) {
        cout << "You get purple\n";
    }
    else if ((color0 == "red" && color1 == "yellow")
    || (color0 == "yellow" && color1 == "red")) {
        cout << "You get orange\n";
    }
    else if ((color0 == "blue" && color1 == "yellow")
    || (color0 == "yelow" && color1 == "blue")) {
        cout << "You get green\n";
    }
    else {
        cout << "Error!!! Color not found\n";
    }
    
}