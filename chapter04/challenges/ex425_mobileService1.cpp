/*
Mobile Service Provider
A mobile phone service provider has three different subscription packages for 
its customers:

Package A:  For $39.99 per month 450 minutes are provided. Additional minutes 
            are $0.45 per minute.

Package B:  For $59.99 per month 900 minutes are provided. Additional minutes 
            are $0.40 per minute.

Package C:  For $69.99 per month unlimited minutes provided.

Write a program that calculates a customer’s monthly bill. It should ask which 
package the customer has purchased and how many minutes were used. It should 
then display the total amount due.

Input Validation: Be sure the user only selects package A, B, or C.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double PKG_A_PRICE = 39.99;
    constexpr double PKG_B_PRICE = 59.99;
    constexpr double PKG_C_PRICE = 69.99;

    constexpr int PKG_A_MINS = 450;
    constexpr int PKG_B_MINS = 900;

    constexpr double PKG_A_EXTRA = 0.45;
    constexpr double PKG_B_EXTRA = 0.40;

    char choice = ' ';
    double number_minutes = 0;
    double additional_minutes = 0;
    double total_cost = 0;

    cout << "Which package are you using: ";
    cin >> choice;

    cout << "How many minutes were used?:  ";
    cin >> number_minutes;

    cout << "Calculating ...\n\n";

    if (choice == 'A' || choice == 'a') {
        if (number_minutes > PKG_A_MINS) {
            additional_minutes = PKG_A_EXTRA * (number_minutes - PKG_A_MINS);
            total_cost = additional_minutes + PKG_A_PRICE;
        }
        else {
            total_cost = PKG_A_PRICE;
        }
    }
    else if (choice == 'B' || choice == 'b') {
        if (number_minutes > PKG_B_MINS) {
            additional_minutes = PKG_B_EXTRA * (number_minutes - PKG_B_MINS);
            total_cost = additional_minutes + PKG_B_PRICE;
        }
        else {
            total_cost = PKG_B_PRICE;
        }
    }
    else if (choice == 'C' || choice == 'c') {
        total_cost = PKG_C_PRICE;
    }
    else {
        cerr << "Incorrect input. Try AGAIN!!!\n";
        exit(1);
    }


    cout << fixed << setprecision(2);
    cout << "Total cost: $" << total_cost << endl;

    return 0;
}