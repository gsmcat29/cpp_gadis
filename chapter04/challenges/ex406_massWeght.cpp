/*
Scientists measure an object’s mass in kilograms and its weight in newtons. If 
you know the amount of mass that an object has, you can calculate its weight, 
in newtons, with the following formula:

Weight = mass X 9.8

Write a program that asks the user to enter an object’s mass, and then 
calculates and displays its weight. If the object weighs more than 1,000 newtons, 
display a message indicating that it is too heavy. If the object weighs less 
than 10 newtons, display a message indicating that the object is too light.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double GRAVITY_CONST = 9.8;
    double weight = 0;
    double mass = 0;

    cout << "Enter object's mass:\t";
    cin >> mass;
    
    weight = mass * GRAVITY_CONST;

    cout << "Object's weight is " << weight << " newtons\n";

    if (weight > 1000) {
        cout << "This object is too heavy!\n";
    }
    else {
        cout << "This object is too light\n";
    }

    return 0;
}