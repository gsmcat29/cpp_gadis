/*
Serendipity Booksellers has a book club that awards points to its customers 
based on the number of books purchased each month. The points are awarded as 
follows:

• If a customer purchases 0 books, he or she earns 0 points.
• If a customer purchases 1 book, he or she earns 5 points.
• If a customer purchases 2 books, he or she earns 15 points.
• If a customer purchases 3 books, he or she earns 30 points.
• If a customer purchases 4 or more books, he or she earns 60 points.

Write a program that asks the user to enter the number of books that he or she 
has purchased this month and then displays the number of points awarded.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr int POINTS_0 = 0;
    constexpr int POINTS_1 = 5;
    constexpr int POINTS_2 = 15;
    constexpr int POINTS_3 = 30;
    constexpr int POINTS_4 = 60;

    int number_books = 0;

    cout << "Enter number of books purchased this month: ";
    cin >> number_books;

    if (number_books >= 4) {
        cout << "You earned " << POINTS_4 << " points\n";
    }
    else if (number_books == 3) {
        cout << "You earned " << POINTS_3 << " points\n";
    }
    else if (number_books == 2) {
        cout << "You earned " << POINTS_2 << " points\n";
    }
    else if (number_books == 1) {
        cout << "You earned " << POINTS_1 << " points\n";
    }
    else {
        cout << "You earned " << POINTS_0 << " points\n";
    }

    return 0;
}