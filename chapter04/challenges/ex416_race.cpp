/*
Running the race
Write a program that asks for the names of three runners and the time it took 
each of them to finish a race. The program should display who came in first, 
second, and third place.
Input Validation: Only accept positive numbers for the times.
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
    string runnerA, runnerB, runnerC;
    int time_a = 0, time_b = 0, time_c = 0;
    int hrA = 0, minA = 0, segA = 0;
    int hrB = 0, minB = 0, segB = 0;
    int hrC = 0, minC = 0, segC = 0;

    cout << "Enter runner A name: ";
    getline(cin, runnerA);
    cout << "Enter time minutes seconds: ";
    cin >> hrA >> minA >> segA;
    cin.get();
    cout << "Enter runner B name: ";
    getline(cin, runnerB);
    cout << "Enter time minutes seconds: ";
    cin >> hrB >> minB >> segB;
    cin.get();
    cout << "Enter runner C name: ";
    getline(cin, runnerC);
    cout << "Enter time minutes seconds: ";
    cin >> hrC >> minC >> segC;        

    cout << "Waiting for results ...\n";

    // convert everything to seconds, for final time
    hrA = hrA * 3600;
    minA = minA * 60;
    time_a = hrA + minA + segA;

    hrB = hrB * 3600;
    minB = minB * 60;
    time_b = hrB + minB + segB;

    hrC = hrC * 3600;
    minC = minC * 60;
    time_c = hrC + minC + segC;

    // compare three times
    if (time_a <= time_b && time_a <= time_c) {
        cout << "Winner is " << runnerA << endl;
    }
    else if (time_b <= time_a && time_b <= time_c) {
        cout << "Winner is " << runnerB << endl;
    }
    else {
        cout << "Winner is " << runnerC << endl;
    }
    
    return 0;
}
