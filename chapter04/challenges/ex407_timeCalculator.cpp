/*Time Calculator
Write a program that asks the user to enter a number of seconds.

• There are 60 seconds in a minute. If the number of seconds entered by the user 
is greater than or equal to 60, the program should display the number of minutes 
in that many seconds.
• There are 3,600 seconds in an hour. If the number of seconds entered by the 
user is greater than or equal to 3,600, the program should display the number of 
hours in that many seconds.
• There are 86,400 seconds in a day. If the number of seconds entered by the 
user is greater than or equal to 86,400, the program should display the number 
of days in that many seconds.
*/

#include <iostream>
using namespace std;

int main()
{
    int seconds = 0;
    int minutes = 0;
    int hours = 0;
    int days = 0;

    int remain_seconds = 0;

    cout << "Enter number of seconds:\t";
    cin >> seconds;

    if (seconds >= 60 && seconds < 3600) {
        minutes = seconds / 60;
        remain_seconds = seconds % 60;

        cout << "Equivalent to:\n";
        cout << minutes << " minute(s), " << remain_seconds << " second(s)\n";
    }
    else if (seconds >= 3600 &&seconds < 86400){
        hours = seconds / 3600;
        remain_seconds = seconds % 3600;

        seconds = remain_seconds;
        //remain_seconds
        minutes = seconds / 60;
        remain_seconds = seconds % 60;


        cout << "Equivalent to:\n";
        cout << hours << " hour(s), " << minutes << " minute(s) " << remain_seconds;
        cout << " second(s)\n";
    }
    else if (seconds >= 86400) {
        days = seconds / 86400;
        remain_seconds = seconds % 3600;

        seconds = remain_seconds;

        hours = seconds / 3600;
        remain_seconds = seconds % 3600;

        seconds = remain_seconds;

        minutes = seconds / 60;
        remain_seconds = seconds % 60;

        cout << "Equivalent to:\n";
        cout << days << " day(s), "  << hours << " hour(s), ";
        cout << minutes << "minute(s), " << remain_seconds << " second(s)\n";
    }


    return 0;
}