/*
Roman Numeral Converter
Write a program that asks the user to enter a number within the range of 1 
through 10. Use a switch statement to display the Roman numeral version of that 
number.
Input Validation: Do not accept a number less than 1 or greater than 10.
*/

#include <iostream>
using namespace std;

int main()
{
    int number = 0;
    

    cout << "Enter a number within the range 1 ~ 10:\t";
    cin >> number;

    if (number < 1 || number > 10) {
        cout << "Value of range\n";
        return 0;
    }

    switch (number)
    {
    case 1:
        cout << "I" << endl;
        break;
    case 2:
        cout << "II" << endl;
        break;
    case 3:
        cout << "III" << endl;
        break;
    case 4:
        cout << "IV" << endl;
        break;
    case 5:
        cout << "V" << endl;
        break;
    case 6:
        cout << "VI" << endl;
        break;
    case 7:
        cout << "VII" << endl;
        break;
    case 8:
        cout << "VIII" << endl;
        break;
    case 9:
        cout << "IX" << endl;
        break;
    case 10:
        cout << "X" << endl;
        break;
    default:
        cout << "Value out of range" << endl;
        break;
    }

    return 0;
}