/*
Create a change-counting game that gets the user to enter the number of coins 
required to make exactly one dollar. The program should ask the user to enter 
the number of pennies, nickels, dimes, and quarters. If the total value of the 
coins entered is equal to one dollar, the program should congratulate the user 
for winning the game. Otherwise, the program should display a message indicating 
whether the amount entered was more than or less than one dollar.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double  TOTAL_CENTS = 100;
    double pennies = 0, nickels = 0, dimes = 0, quarters = 0;
    double total_value = 0;

    cout << "Enter amount of pennies:  \t";
    cin >> pennies;
    cout << "Enter amount of nickels:  \t";
    cin >> nickels;
    cout << "Enter amount of dimes:    \t";
    cin >> dimes;
    cout << "Enter amount of quarters: \t";
    cin >> quarters;

    total_value = pennies + nickels + dimes + quarters;

    if (total_value == TOTAL_CENTS) {
        cout << "Congratulations!! You win a dollar\n";
    }
    else if (total_value < TOTAL_CENTS){
        cout << "The entered amount is less than one dollar\n";
    }
    else {
        cout << "The entered amount is more than one dollar\n";
    }

    return 0;
}