/*
Fat Gram Calculator
Write a program that asks for the number of calories and fat grams in a food. 
The program should display the percentage of calories that come from fat. If the 
calories from fat are less than 30% of the total calories of the food, it should 
also display a message indicating that the food is low in fat.

One gram of fat has 9 calories, so
Calories from fat  = fat grams * 9
The percentage of calories from fat can be calculated as
Calories from fat = total calories

Input Validation: Make sure the number of calories and fat grams are not less 
than 0. Also, the number of calories from fat cannot be greater than the total 
number of calories. If that happens, display an error message indicating that 
either the calories or fat grams were incorrectly entered.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr int CALORIES_PER_FAT = 9;

    double number_calories = 0;
    double fat_grams = 0;
    double calorie_fat = 0;
    double calorie_percentage = 0;
    double total_calories = 0;

    cout << "Enter number of calories: ";
    cin >> number_calories;
    cout << "Enter fat grams: ";
    cin >> fat_grams;

    calorie_fat = fat_grams * CALORIES_PER_FAT;
    total_calories = number_calories + calorie_fat;
    calorie_percentage = calorie_fat / total_calories;

    if (calorie_fat > total_calories) {
        cout << "Input error.\n";
        cout << "Either the calories or fat gram are incorrect\n";
        cout << "Try again\n";
    }

    if (calorie_percentage < (0.3 * total_calories)) {
        cout << "Low fat food\n";
    }

    return 0;
}