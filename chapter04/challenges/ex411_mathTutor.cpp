/*
Math Tutor
This is a modification of Programming Challenge 17 from Chapter 3. Write a 
program that can be used as a math tutor for a young student. The program should 
display two random numbers that are to be added, such as:

 247
+129

The program should wait for the student to enter the answer. If the answer is 
correct, a message of congratulations should be printed. If the answer is 
incorrect, a message should be printed showing the correct answer.
*/

#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{   
    constexpr int MIN_VALUE = 100;
    constexpr int MAX_VALUE = 999;

    int value1 = 0;
    int value2 = 0;
    int answer = 0;
    int result = 0;

    cout << "Answer the exercise. When you finish type ENTER:\n";
    // Use srand directly with time(0)
    srand(time(0));

    // or use a variable to hold the value
    // unsigned seed = time(0);
    // srand(seed);

    // using rand directly generate big random numbers
    // value1 = rand();
    // value2 = rand();

    // to limit from 100 to 999
    value1 = (rand() % (MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    value2 = (rand() % (MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    answer = value1 + value2;

    cout << "  " << value1 << '\n';
    cout << "+ " << value2 << '\n';
    cout << "\n\n";
    cout << "Type answer, then press ENTER: ";
    cin >> result;
    cout << "Calculating ...\n";

    // wait for student to press a key
    cin.get();

    // display complete result


    if (result == answer) {
        cout << "CONGRATS. CORRECT ANSWER!!!\n";
    }
    else {
        cout << "INCORRECT\n";
        cout << "  " << value1 << '\n';
        cout << "+ " << value2 << '\n';
        cout << "----\n";        
        cout << answer << endl;
    }

    return 0;
}