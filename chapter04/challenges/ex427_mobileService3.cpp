/*
Mobile Service Provider, Part 3
Months with 30 days have 720 hours, and months with 31 days have 744 hours.
February, with 28 days, has 672 hours. You can calculate the number of minutes 
in a month by multiplying its number of hours by 60. Enhance the input 
validation of the Mobile Service Provider program by asking the user for the 
month (by name), and validating that the number of minutes entered is not more 
than the maximum for the entire month. Here is a table of the months, their days, 
and number of hours in each. 

Month                       Days                        Hours
January                     31                          744
February                    28                          672
March                       31                          744
April                       30                          720
May                         31                          744
June                        30                          720
July                        31                          744
August                      31                          744
September                   30                          720
October                     31                          744
November                    30                          720
December                    31                          744
*/

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
    constexpr double PKG_A_PRICE = 39.99;
    constexpr double PKG_B_PRICE = 59.99;
    constexpr double PKG_C_PRICE = 69.99;

    constexpr int PKG_A_MINS = 450;
    constexpr int PKG_B_MINS = 900;

    constexpr double PKG_A_EXTRA = 0.45;
    constexpr double PKG_B_EXTRA = 0.40;

    constexpr int MINUTES_28_DAYS = 40320;
    constexpr int MINUTES_30_DAYS = 43200;
    constexpr int MINUTES_31_DAYS = 44640;
    //=========================================================================
    char choice = ' ';
    double number_minutes = 0;
    double additional_minutes = 0;
    double total_cost = 0;

    string month = " ";

    cout << "Which package are you using: ";
    cin >> choice;

    cout << "How many minutes were used?:  ";
    cin >> number_minutes;

    cout << "Enter month name (lowercase): ";
    cin >> month;

    cout << "Calculating ...\n\n";

    // validate max minutes
    if (choice == 'A') {

        if (month == "february" && number_minutes > MINUTES_28_DAYS) {
            cerr << "Incorrect amount of minutes\n";
            exit(1);
        }
        else if ((month == "april" && number_minutes > MINUTES_30_DAYS) || 
                (month == "june" && number_minutes > MINUTES_30_DAYS) ||
                (month == "september" && number_minutes > MINUTES_30_DAYS) ||
                (month == "november" && number_minutes > MINUTES_30_DAYS)) {
            cerr << "Incorrect amount of minutes\n";
            exit(1);
        }
        else if ((month == "january" && number_minutes > MINUTES_31_DAYS) || 
                (month == "march" && number_minutes > MINUTES_31_DAYS) ||
                (month == "may" && number_minutes > MINUTES_31_DAYS) ||
                (month == "july" && number_minutes > MINUTES_31_DAYS) || 
                (month == "august" && number_minutes > MINUTES_31_DAYS) ||
                (month == "october" && number_minutes > MINUTES_31_DAYS) ||
                (month == "december" && number_minutes > MINUTES_31_DAYS)) {
            cerr << "Incorrect amount of minutes\n";
            exit(1);
        }  
    }
    else if (choice == 'B')
    {
        if (month == "february" && number_minutes > MINUTES_28_DAYS) {
            cerr << "Incorrect amount of minutes\n";
            exit(1);
        }
        else if ((month == "april" && number_minutes > MINUTES_30_DAYS) || 
                (month == "june" && number_minutes > MINUTES_30_DAYS) ||
                (month == "september" && number_minutes > MINUTES_30_DAYS) ||
                (month == "november" && number_minutes > MINUTES_30_DAYS)) {
            cerr << "Incorrect amount of minutes\n";
            exit(1);
        }
        else if ((month == "january" && number_minutes > MINUTES_31_DAYS) || 
                (month == "march" && number_minutes > MINUTES_31_DAYS) ||
                (month == "may" && number_minutes > MINUTES_31_DAYS) ||
                (month == "july" && number_minutes > MINUTES_31_DAYS) || 
                (month == "august" && number_minutes > MINUTES_31_DAYS) ||
                (month == "october" && number_minutes > MINUTES_31_DAYS) ||
                (month == "december" && number_minutes > MINUTES_31_DAYS)) {
            cerr << "Incorrect amount of minutes\n";
            exit(1);
        }  
    }


    if (choice == 'A' || choice == 'a') {
        if (number_minutes > PKG_A_MINS) {
            additional_minutes = PKG_A_EXTRA * (number_minutes - PKG_A_MINS);
            total_cost = additional_minutes + PKG_A_PRICE;
        }
        else {
            total_cost = PKG_A_PRICE;
        }
    }
    else if (choice == 'B' || choice == 'b') {
        if (number_minutes > PKG_B_MINS) {
            additional_minutes = PKG_B_EXTRA * (number_minutes - PKG_B_MINS);
            total_cost = additional_minutes + PKG_B_PRICE;
        }
        else {
            total_cost = PKG_B_PRICE;
        }
    }
    else if (choice == 'C' || choice == 'c') {
        total_cost = PKG_C_PRICE;
    }
    else {
        cerr << "Incorrect input. Try AGAIN!!!\n";
        exit(1);
    }

    cout << fixed << setprecision(2);
    cout << "Total cost: $" << total_cost << endl;

    return 0;
}