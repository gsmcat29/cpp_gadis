// This program demonstrates a variable defined in an inner block
#include <iostream>
using namespace std;

int main()
{
    // constants for minimum income and years
    const double MIN_INCOME = 35000.0;
    const int MIN_YEARS = 5;

    // get the annual income
    cout << "What is your annual income? ";
    double income;          // variable definition
    cin >> income;

    if (income >= MIN_YEARS) {
        // get the number of years at the current job
        cout << "How many years have you worked at your current job? ";
        int years;             // variable definiton
        cin >> years;
        
        if (years > MIN_YEARS)
            cout << "You qualify\n";
        else {
            cout << "You must have been employed for mora than " << MIN_YEARS
                 << " years to qualify\n";
        }
    }
    else {
        cout << "You must earn at least $" << MIN_INCOME << " or have been "
            << "employed more than " << MIN_YEARS << " years\n";       
    }

    return 0;
}