// this program uses nested if/else statements to assign a letter grade
// (A, B, C, D, or F) to a numeric test score 
#include <iostream>
using namespace std;

int main()
{
    // constants for grade thresholds
    const int A_SCORE = 90,
              B_SCORE = 80,
              C_SCORE = 70,
              D_SCORE = 60;
    
    int testScore;  // to hold a numeric test score

    // get the numeric test score
    cout << "Enter your numeric test score and I will\n";
    cout << "tell you the letter grde you earned: ";
    cin >> testScore;

    // Determine the letter score
    if (testScore >= A_SCORE) {
        cout << "Your grade is A\n";
    }
    else {
        if (testScore >= B_SCORE) {
            cout << "Your gradee is B\n";
        }
        else {
            if (testScore >= C_SCORE) {
                cout << "Your grade is C\n";
            }
            else {
                if (testScore >= D_SCORE) {
                    cout << "Your grade is D\n";
                }
                else {
                    cout << "Your grade is F\n";
                }
            }
        }
    }

    return 0;
}