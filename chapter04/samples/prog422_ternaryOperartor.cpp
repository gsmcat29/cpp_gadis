// This program calculates a consultant's charges at $50 per hour, for a
// minimum of 5 hours. The ?: operator adjusts hours to 5 if less than 5 hours
// were worked
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    const double PAY_RATE = 50.0;       // HOURLY PAY RAtE
    const int MIN_HOURS = 5;            // minimum billable hours
    double hours,                       // hours worked
           charges;                     // total charges
    
    // get the hours worked
    cout << "How many hours were worked? ";
    cin >> hours;

    // determine the hours to charge for
    hours = hours < MIN_HOURS ? MIN_HOURS : hours;

    // calculate and display the charges
    charges = PAY_RATE * hours;
    cout << fixed << showpoint << setprecision(2)
         << "The charges are $" << charges << endl;
    
    return 0;
}