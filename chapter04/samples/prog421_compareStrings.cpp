// This program uses relational operators to compare string entered by the user
// with valid stereo part numbers
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
    const double PRICE_A = 249.0,
                 PRICE_B = 299.0;
    
    string partNum;     // holds a stereo part number

    /// display available part and get the user's selection
    cout << "The stereo part numbers are:\n"
         << "Boom Box: part number S-29A \n"
         << "Shlef Model: part number S-29B \n"
         << "Enter the part number of the stereo you wish to purchase: ";
    
    cin >> partNum;

    // set the numeric output formatting
    cout << fixed << setprecision(2);

    // determine and display the correct price
    if (partNum == "S-29A")
        cout << "The price is $" << PRICE_A << endl;
    else if (partNum == "S-29B")
        cout << "The price is $" << PRICE_B << endl;
    else
        cout << partNum << " is not a valid part number\n";
    
    return 0;
}