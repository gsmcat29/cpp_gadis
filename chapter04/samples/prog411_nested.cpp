// This program demonstrates the nested if statement
#include <iostream>
using namespace std;

int main()
{
    char employed,      // currently employed, Y or N
         recentGrad;    // Recent graduate, Y or N
    
    // Is the user employed and a recent graduate?
    cout << "Answer the following questions with either Y for yes or N for no\n";
    cout << "Are you employed? ";
    cin >> employed;
    cout << "Have you graduated from college in the past two years? ";
    cin >> recentGrad;

    // Determine the user's loan qualifications
    if (employed == 'Y') {
        if (recentGrad == 'Y') {
            cout << "You qualify for the special interest rate\n";
        }
        else {
            cout << "You must have graduated from college in the past two\n";
            cout << "years to qualify\n";
        }
    }
    else {
        cout << "You must be employed to qualify\n";
    }
    
    return 0;    
}