// This program averages 3 test scores. The if statement uses the = operator,
// but the == operator was intended
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int score1, score2, score3;     // to hold three test scores
    double average;                 // to hold the average score

    // get the three test scores
    cout << "Enter 3 scores and I will average them: ";
    cin >> score1 >> score2 >> score3;

    // calculate and display the average score
    average = (score1 + score2 + score3) / 3.0;
    cout << fixed << showpoint << setprecision(1);
    cout << "Your average is " << average << endl;

    // our attention is to congratulate the user for having perfect score.
    // But, this doesn't work.
    if (average = 100) // WRONG! This is an assigment!
        cout <<  "Congratulations! That's a perfect score!\n";
    
    return 0;
}