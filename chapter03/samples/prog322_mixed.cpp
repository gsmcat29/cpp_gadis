// this program demonstrates a problem that occurs when you mix cin >> cin.get()
#include <iostream>
using namespace std;

int main()
{
    char ch;        // define a character variable
    int number;     // define a integer variable

    cout << "Enter a number: ";
    cin >> number;  // read an integer
    cout << "Enter a character: ";
    ch = cin.get();
    cout << "Thank You!\n";

    return 0;
}