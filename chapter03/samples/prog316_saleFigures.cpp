// This program asks for sales figures for 3 days. The total sales are calculated
// anmd displayed in a table
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double day1, day2, day3, total;

    // get the sales for each day
    cout << "Enter the sales for day1: ";
    cin >> day1;

    // get the sales for each day
    cout << "Enter the sales for day2: ";
    cin >> day2;

    // get the sales for each day
    cout << "Enter the sales for day3: ";
    cin >> day3;

    // calculate the total sales
    total = day1 + day2 + day3;

    // display the total sales
    cout << "\nSales Figures\n";
    cout << "---------------\n";
    cout << setprecision(5);
    cout << "Day 1: " << setw(8) << day1 << endl;
    cout << "Day 2: " << setw(8) << day2 << endl;
    cout << "Day 3: " << setw(8) << day3 << endl;
    cout << "Total: " << setw(8) << total << endl;

    return 0;
}