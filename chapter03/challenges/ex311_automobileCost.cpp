/*
Automobile Costs
Write a program that asks the user to enter the monthly costs for the following
expenses incurred from operating his or her automobile: loan payment, insurance, 
gas, oil, tires, and maintenance. The program should then display the total 
monthly cost of these expenses, and the total annual cost of these expenses.
*/
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double loan_payment = 0;
    double insurance = 0;
    double gas = 0;
    double oil = 0;
    double tires = 0;
    double maintenance = 0;
    double monthly_cost = 0;
    double yearly_cost = 0;

    cout << "Enter the expenses of the following items: \n\n";
    cout << "Loan Payment: ";
    cin >> loan_payment;

    cout << "Insurance: ";
    cin >> insurance;

    cout << "Gas: ";
    cin >> gas;

    cout << "Oil: ";
    cin >> oil;

    cout << "Tires: ";
    cin >> tires;

    cout << "Maintenance: ";
    cin >> maintenance;

    monthly_cost = loan_payment + insurance + gas + oil + tires + maintenance;

    cout << "\nMonthly Expense: $";
    cout << setprecision(2) << fixed;
    cout << setw(9) << monthly_cost << '\n';

    cout << "Yearly Expense:  $";
    yearly_cost = monthly_cost * 12;
    cout << setw(9) << yearly_cost << endl;

    return 0;
}

