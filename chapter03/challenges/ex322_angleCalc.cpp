/*
Angle Calculator
Write a program that asks the user for an angle, entered in radians. The program
should then display the sine, cosine, and tangent of the angle. (Use the sin, 
cos, and tan library functions to determine these values.) The output should be 
displayed in fixed-point notation, rounded to four decimal places of precision.
*/

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main()
{
    double angle_radians = 0;
    double sin_output = 0;
    double cos_output = 0;
    double tan_output = 0;

    cout << "Enter angle (radians): ";
    cin >> angle_radians;

    sin_output = sin(angle_radians);
    cos_output = cos(angle_radians);
    tan_output = tan(angle_radians);

    cout << setprecision(4) << fixed;
    cout << "sin (" << angle_radians << ") = " << setw(9) << sin_output << endl;
    cout << "cos (" << angle_radians << ") = " << setw(9) << cos_output << endl;
    cout << "tan (" << angle_radians << ") = " << setw(9) << tan_output << endl;

    return 0;
}