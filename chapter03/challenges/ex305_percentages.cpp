/*
Male and Female Percentages
Write a program that asks the user for the number of males and the number of 
females registered in a class. The program should display the percentage of 
males and females in the class.
Hint: Suppose there are 8 males and 12 females in a class. There are 20 students 
in the class. The percentage of males can be calculated as 8 ÷ 20 = 0.4, or 40%. 
The percentage of females can be calculated as 12 ÷ 20 = 0.6, or 60%.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int number_males = 0;
    int number_females = 0;
    double total_students = 0;
    double percentage_females = 0;
    double percentage_males = 0;

    cout << "Type number of females: ";
    cin >> number_females;
    cout << "Type number of males: ";
    cin >> number_males;

    total_students = number_females + number_males;

    percentage_females = number_females / total_students;
    percentage_males = number_males / total_students;

    cout << "Female percentage = " <<  (percentage_females * 100) << "% \n";
    cout << "Male percentage = " << (percentage_males * 100) << "% \n";
    cout << endl;

    return 0;
}