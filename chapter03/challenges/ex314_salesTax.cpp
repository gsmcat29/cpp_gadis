/*
Monthly Sales Tax
A retail company must file a monthly sales tax report listing the sales for the 
month and the amount of sales tax collected. Write a program that asks for the 
month, the year, and the total amount collected at the cash register (that is, 
sales plus sales tax). Assume the state sales tax is 4 percent and the county 
sales tax is 2 percent.

If the total amount collected is known and the total sales tax is 6 percent, the amount
of product sales may be calculated as:

S = T / 1.06

S is the product sales and T is the total income (product sales plus sales tax).
The program should display a report similar to

Month: October
--------------------
Total Collected:                $ 26572.89
Sales:                          $ 25068.76
County Sales Tax:               $   501.38
State Sales Tax:                $ 1002.75
Total Sales Tax:                $ 1504.13

*/

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
    constexpr double STATE_TAX = 0.04;
    constexpr double COUNTY_TAX = 0.02;
    constexpr double SALES_TAX = 1.06;

    string month = "";
    double sales = 0;
    double total_collected = 0;
    double county_sales = 0;
    double state_sales = 0;
    double total_sales = 0;

    cout << "Type month name: ";
    cin >> month;

    cout << "Type total collected: ";
    cin >> total_collected;

    cout << "Month: " << month << '\n';
    cout << "----------------------\n";

    cout << setprecision(2) << fixed;
    cout << "Total Collected: \t$" << setw(9) << total_collected << '\n';

    sales = total_collected / SALES_TAX;
    cout << "Sales:           \t$" << setw(9) << sales << '\n';

    county_sales = sales * COUNTY_TAX;
    state_sales = sales * STATE_TAX;

    cout << "County Sales Tax:\t$" << setw(9) << county_sales << '\n';
    cout << "State Sales Tax: \t$" << setw(9) << state_sales << '\n';

    total_sales = county_sales + state_sales;
    cout << "Total Sales Tax: \t$" << setw(9) << total_sales << endl;

    return 0;
}