/*
Word Game
Write a program that plays a word game with the user. The program should ask the
user to enter the following:

• His or her name
• His or her age
• The name of a city
• The name of a college
• A profession
• A type of animal
• A pet’s name

After the user has entered these items, the program should display the following 
story, inserting the user’s input into the appropriate locations:

There once was a person named NAME who lived in CITY. At the age of
AGE, NAME went to college at COLLEGE. NAME graduated and went to work
as a PROFESSION. Then, NAME adopted a(n) ANIMAL named PETNAME. They
both lived happily ever after!

*/
#include <iostream>
#include <string>
using namespace std;

int main()
{
    string name;
    string age;
    string city;
    string college;
    string profession;
    string animal_type;
    string pet_name;

    cout << "Enter name: ";
    cin >> name;
    cout << "Enter age: ";
    cin >> age;
    cout << "Enter city name->";
    cin.ignore(); 
    getline(cin, city);
    cout << "Enter college: ";
    getline(cin, college);
    cout << "Enter profession: ";
    getline(cin, profession);
    cout << "Enter type of animal: ";
    cin >> animal_type;
    cout << "Enter per's name: ";
    cin >> pet_name;

    cout << "\n\n\n\n\n";
    cout << "There once was a person named " << name << " who lived in " << city;
    cout << " At the age of\n"  << age << ", " << name << " went to college at";
    cout << " " << college << "." << " " << name << "graduated and went to work\n";
    cout << "as a " << profession << ". Then, " << name << " adopted a(n) ";
    cout << animal_type << " named " << pet_name << ". They\n";
    cout << "both lived happily ever after!" << endl;

    return 0;
}