/*
Box Office
A movie theater only keeps a percentage of the revenue earned from ticket sales. 
The remainder goes to the movie distributor. Write a program that calculates a 
theater’s gross and net box office profit for a night. The program should ask 
for the name of the movie, and how many adult and child tickets were sold. 
(The price of an adult ticket is $10.00 and a child’s ticket is $6.00.) 
It should display a report similar to

Movie Name:                             “Wheels of Fury”
Adult Tickets Sold:                         382
Child Tickets Sold:                         127
Gross Box Office Profit:                    $ 4582.00
Net Box Office Profit:                      $  916.40
Amount Paid to Distributor:                 $ 3665.60

NOTE: Assume the theater keeps 20 percent of the gross box office profit.
*/

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
    constexpr double GROSS_PERCENT = 0.2;
    constexpr double ADULT_PRICE = 10.0;
    constexpr double CHILD_PRICE = 6.0;

    string movie_name = "";
    int adult_tickets = 0;
    int child_tickets = 0;

    double amount_paid = 0;
    double net_profit = 0;
    double gross_profit = 0;

    cout << "Enter movie name: ";
    //cin >> movie_name;
    getline(cin, movie_name);

    cout << "Number of adult tickets: ";
    cin >> adult_tickets;

    cout << "Number of child tickets: ";
    cin >> child_tickets;

    cout << setprecision(2) << fixed;

    gross_profit = (ADULT_PRICE * adult_tickets) + (CHILD_PRICE * child_tickets);
    cout << "Gross Box Office Profit:    \t$" << setw(9) << gross_profit << '\n';

    net_profit = GROSS_PERCENT * gross_profit;
    cout << "Net Box Office Profit:      \t$" << setw(9) << net_profit << '\n';

    amount_paid = gross_profit - net_profit;
    cout << "Amount Paid to Distributor: \t$" << setw(9) << amount_paid << endl;
    
    return 0;
}