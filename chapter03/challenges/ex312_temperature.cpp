/*
Celsius to Fahrenheit
Write a program that converts Celsius temperatures to Fahrenheit temperatures. The
formula is

F = (9/5)C + 32

F is the Fahrenheit temperature, and C is the Celsius temperature.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double celsius = 0;
    double fahrenheit = 0;

    cout << "Enter temperature in Celsius: ";
    cin >> celsius;

    fahrenheit = (9 * celsius) / 5 + 32;

    cout << "Temperature conversion: " << fahrenheit << endl;

    return 0;
}
