/*
Property Tax
A county collects property taxes on the assessment value of property, which is 
60 percent of the property’s actual value. If an acre of land is valued at 
$10,000, its assessment value is $6,000. The property tax is then 75¢ for each 
$100 of the assessment value.
The tax for the acre assessed at $6,000 will be $45. Write a program that asks 
for the actual value of a piece of property and displays the assessment value 
and property tax.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double PROPERTY_TAX = 0.60;
    constexpr double TAX_IN_CENTS = 0.75;

    double land_value = 0;
    double assessment_value = 0;
    double tax_value = 0;

    cout << "Enter land value: ";
    cin >> land_value;

    assessment_value = land_value * PROPERTY_TAX;
    tax_value = (assessment_value * TAX_IN_CENTS) / 100.0;

    cout << setprecision(2) << fixed;
    cout << '\n';
    cout << "Assessment value:      $" << setw(9) << assessment_value << '\n';
    cout << "Property tax for land: $" << setw(9) << tax_value << endl;

    return 0;
}