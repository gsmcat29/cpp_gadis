/*
How Many Pizzas?
Modify the program you wrote in Programming Challenge 18 (Pizza Pi) so that it
reports the number of pizzas you need to buy for a party if each person 
attending is expected to eat an average of four slices. The program should ask 
the user for the number of people who will be at the party and for the diameter 
of the pizzas to be ordered. It should then calculate and display the number of 
pizzas to purchase.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double PI = 3.14159;
    constexpr double SLICE_AREA = 14.125;
    constexpr int SLICE_AVG = 4;

    double number_people = 0;
    double diameter = 0;
    double radius = 0;
    double number_slices = 0;
    double area = 0;
    
    int min_slices = 0;

    cout << "Enter pizza diameter (inches): ";
    cin >> diameter;
    cout << "Enter number of people: ";
    cin >> number_people;

    radius = diameter / 2.0;
    area = PI * radius * radius;
    number_slices = area / SLICE_AREA;


    min_slices = SLICE_AVG * number_people;

    cout << setprecision(2) << fixed;
    cout << "The number of slices is " << number_slices << endl;
    cout << "The number of pizzas are " << min_slices / number_slices << endl;
    /* The number of slices depends on the size of a pizza */
    
    return 0;
}