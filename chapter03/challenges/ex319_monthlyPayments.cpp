/*
Monthly Payments
The monthly payment on a loan may be calculated by the following formula:

Payment = Rate * ((1 + Rate)^N / ((1 + Rate)N - 1) )* L

Rate is the monthly interest rate, which is the annual interest rate divided by 
12. (12% annual interest would be 1 percent monthly interest.) N is the number 
of payments, and L is the amount of the loan. Write a program that asks for 
these values and displays a report similar to


Loan Amount:                            $ 10000.00
Monthly Interest Rate:                          1%
Number of Payments:                             36
Monthly Payment:                        $   332.14
Amount Paid Back:                       $ 11957.15
Interest Paid:                          $ 1957.15

*/

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main()
{
    double loan_amount = 0;             // L
    double rate = 0;
    int number_payments = 0;            // N
    double monthly_payment = 0;
    double amount_paid = 0;
    double interest_paid = 0;
    double payment_denominator = 0;
    double payment_numerator = 0;

    cout << "Enter loan amount: ";
    cin >> loan_amount;
    cout << "Enter monthly interest rate: ";
    cin >> rate;
    cout << "Enter number of payments: ";
    cin >> number_payments;

    // num / dem
    rate = rate / 100.0;
    payment_numerator = rate * pow((1+rate), number_payments);
    payment_denominator = pow((1+rate), number_payments) - 1;
    
    monthly_payment = (payment_numerator / payment_denominator) * loan_amount;
    amount_paid = monthly_payment * number_payments;
    interest_paid = amount_paid - loan_amount;

    // Display report
    cout << "\n\n*** REPORT ***\n";
    cout << setprecision(2) << fixed;
    cout << "Loan Amount:           \t$ " << setw(9) << loan_amount << '\n';
    cout << "Monthly Interest Rate: \t  " << setw(9) << (int ) (rate * 100) << '\n';
    cout << "Number of payments:    \t  " << setw(9) << number_payments << '\n';
    cout << "Monthly Payment:       \t$ " << setw(9) << monthly_payment << '\n';
    cout << "Amount Paid Back:      \t$ " << setw(9) << amount_paid << '\n';
    cout << "Interest Paid:         \t$ " << setw(9) << interest_paid << endl;
    
    return 0;
}

