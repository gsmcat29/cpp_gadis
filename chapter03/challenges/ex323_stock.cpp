/*
Stock Transaction Program
Last month Joe purchased some stock in Acme Software, Inc. Here are the details 
of the purchase:

• The number of shares that Joe purchased was 1,000.
• When Joe purchased the stock, he paid $45.50 per share.
• Joe paid his stockbroker a commission that amounted to 2% of the amount he paid
for the stock.

Two weeks later Joe sold the stock. Here are the details of the sale:

• The number of shares that Joe sold was 1,000.
• He sold the stock for $56.90 per share.
• He paid his stockbroker another commission that amounted to 2% of the amount
  he received for the stock.

Write a program that displays the following information:

• The amount of money Joe paid for the stock.
• The amount of commission Joe paid his broker when he bought the stock.
• The amount that Joe sold the stock for.
• The amount of commission Joe paid his broker when he sold the stock.
• Display the amount of profit that Joe made after selling his stock and paying 
  the two commissions to his broker. (If the amount of profit that your program 
  displays is a negative number, then Joe lost money on the transaction.)
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr int SHARE_PURCHASE = 1000;
    constexpr double COMMISSION_PURCHASE = 0.02;
    constexpr double STOCK_PURCHASE = 45.50;
    
    constexpr int SHARE_SOLD = 1000;
    constexpr double STOCK_SOLD = 56.90;
    constexpr double COMMISSION_SOLD = 0.02;

    double paid_stock = 0;
    double paid_stock_commission = 0;
    double sold_stock = 0;
    double sold_stock_commission = 0;
    double profit_amount = 0;

    cout << "Display results:\n\n";

    paid_stock = SHARE_PURCHASE * STOCK_PURCHASE;
    paid_stock_commission = paid_stock * COMMISSION_PURCHASE;

    sold_stock = SHARE_SOLD * STOCK_SOLD;
    sold_stock_commission = sold_stock * COMMISSION_SOLD;

    profit_amount = paid_stock_commission + sold_stock_commission + sold_stock;

    cout << setprecision(2) << fixed;
    cout << "Amount paid for stock:       $ " << setw(9) << paid_stock << endl;
    cout << "Commission paid for broker:  $ " << setw(9) << paid_stock_commission;
    cout << endl;
    cout << "Amount sold for stock:       $ " << setw(9) << sold_stock << endl;
    cout << "Commission sold for broker:  $ " << setw(9) << sold_stock_commission;
    cout << endl; 
    cout << "Amount of profit:            $ " << setw(9) << profit_amount << endl;

    return 0;
}