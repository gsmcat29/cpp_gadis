/*
Currency
Write a program that will convert U.S. dollar amounts to Japanese yen and to 
euros, storing the conversion factors in the constants YEN_PER_DOLLAR and 
EUROS_PER_DOLLAR. To get the most up-to-date exchange rates, search the Internet 
using the term “currency exchange rate”. If you cannot find the most recent 
exchange rates, use the following:

1 Dollar = 98.93 Yen
1 Dollar = 0.74 Euros

Format your currency amounts in fixed-point notation, with two decimal places of
precision, and be sure the decimal point is always displayed.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double YEN_PER_DOLLAR = 98.93;
    constexpr double EUROS_PER_DOLLAR = 0.74;

    double amount_dollars = 0;
    double amount_yens = 0;
    double amount_euros = 0;

    cout << "Enter the amount of dollars you have: ";
    cin >> amount_dollars;

    amount_yens = YEN_PER_DOLLAR * amount_dollars;
    amount_euros = EUROS_PER_DOLLAR * amount_dollars;
    
    cout << setprecision(2) << fixed;
    cout << "You have " << amount_yens << " Yen\n";
    cout << "You have " << amount_euros << " Euros" << endl;

    return 0;
}