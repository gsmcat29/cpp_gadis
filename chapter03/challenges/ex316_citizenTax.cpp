/*
Senior Citizen Property Tax
Madison County provides a $5,000 homeowner exemption for its senior citizens. 
For example, if a senior’s house is valued at $158,000 its assessed value would 
be $94,800, as explained above. However, he would only pay tax on $89,800. 
At last year’s tax rate of $2.64 for each $100 of assessed value, the property 
tax would be $2,370.72. In addition to the tax break, senior citizens are 
allowed to pay their property tax in four equal payments. 
The quarterly payment due on this property would be $592.68. Write a program 
that asks the user to input the actual value of a piece of property and the 
current tax rate for each $100 of assessed value. The program should then 
calculate and report how much annual property tax a senior homeowner will be 
charged for this property and what the quarterly tax bill will be.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double PROPERTY_TAX = 0.60;
    constexpr double EXCEPTION_VAL = 5000;
    constexpr double TAX_RATE = 2.64;

    double house_value  = 0;
    double assessment_value = 0;
    double property_tax = 0;
    double quarterly_pay = 0;

    cout << "Type house value: ";
    cin >> house_value;

    assessment_value = (house_value * PROPERTY_TAX) - EXCEPTION_VAL;
    property_tax = assessment_value * (TAX_RATE / 100.0);
    quarterly_pay = property_tax / 4.0;

    cout << "Results report ************************************************\n";
    cout << setprecision(2) << fixed;
    cout << "Assesment Value:\t$" << setw(9) << assessment_value << '\n';
    cout << "Property Tax:   \t$" << setw(9) << property_tax << '\n';
    cout << "Quarterly pay:  \t$" << setw(9) << quarterly_pay << endl;
    
    return 0;
}