/*
Average Rainfall
Write a program that calculates the average rainfall for three months. 
The program should ask the user to enter the name of each month, such as June or 
July, and the amount of rain (in inches) that fell each month. The program 
should display a message similar to the following:
The average rainfall for June, July, and August is 6.72 inches.
*/

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
    string month1;
    string month2;
    string month3;

    double rain_month1;
    double rain_month2;
    double rain_month3;

    double rain_summation = 0;
    double rain_average = 0;

    cout << "Enter month 1: ";
    cin >> month1;
    cout << "Enter month 2: ";
    cin >> month2;
    cout << "Enter month 3: ";
    cin >> month3;

    cout << "Enter amount of rain in " << month1 << " : ";
    cin >> rain_month1;

    cout << "Enter amount of rain in " << month2 << " : ";
    cin >> rain_month2;    

    cout << "Enter amount of rain in " << month3 << " : ";
    cin >> rain_month3;

    rain_summation = rain_month1 + rain_month2 + rain_month3;
    rain_average = rain_summation / 3.0;
    
    cout << setprecision(2) << fixed;
    cout << "The average rainfall for " << month1 << "," << month2;
    cout << " ,and " << month3 << " is " << rain_average << '\n';
    
    return 0;
}