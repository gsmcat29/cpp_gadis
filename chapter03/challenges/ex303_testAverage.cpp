/*
Test Average
Write a program that asks for five test scores. The program should calculate the 
average test score and display it. The number displayed should be formatted in 
fixed-point notation, with one decimal point of precision.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int score1 = 0;
    int score2 = 0;
    int score3 = 0;
    int score4 = 0;
    int score5 = 0;

    double average = 0;
    double sum = 0;

    cout << "Enter score 1: ";
    cin >> score1;

    cout << "Enter score 2: ";
    cin >> score2;

    cout << "Enter score 3: ";
    cin >> score3;

    cout << "Enter score 4: ";
    cin >> score4;

    cout << "Enter score 5: ";
    cin >> score5;

    sum = score1 + score2 +  score3 + score4 + score5;
    average = sum / 5.0;

    cout << setprecision(1) << fixed;
    cout << "Average = " << average << '\n';

    return 0;
}