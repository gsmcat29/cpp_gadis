/*
Miles per Gallon
Write a program that calculates a car’s gas mileage. The program should ask the 
user to enter the number of gallons of gas the car can hold and the number of 
miles it can be driven on a full tank. It should then display the number of 
miles that may be driven per gallon of gas.
*/

#include <iostream>
using namespace std;

int main()
{
    double number_gallons = 0;
    double number_miles_full_tank = 0;
    double driven_miles = 0;

    // ask user to enter number of gallons
    cout  << "Enter number of gallons of gas in the car: ";
    cin >> number_gallons;
    cout << "Enter the number of miles the car can be driven full tank: ";
    cin >> number_miles_full_tank;

    driven_miles = number_miles_full_tank / number_gallons;

    cout << "The total number of miles it can be driven are: " << driven_miles;
    cout << '\n';

    return 0;
}