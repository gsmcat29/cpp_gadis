/*
How Many Widgets?
The Yukon Widget Company manufactures widgets that weigh 12.5 pounds each.
Write a program that calculates how many widgets are stacked on a pallet, based 
on the total weight of the pallet. The program should ask the user how much the 
palletweighs by itself and with the widgets stacked on it. It should then 
calculate and display the number of widgets stacked on the pallet.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double YUKON_WIDGET = 12.5;
    double number_widgets = 0;
    double weight_pallet = 0;
    double weight_with_widgets = 0;

    cout << "How much the pallet weights? ";
    cin >> weight_pallet;

    cout << "How much  the pallet weights with widgets? ";
    cin >> weight_with_widgets;

    number_widgets = (weight_with_widgets - weight_pallet) / YUKON_WIDGET;

    cout << setprecision(2) << fixed;
    cout << "Number of widgets: " << number_widgets << endl;

    return 0;
}