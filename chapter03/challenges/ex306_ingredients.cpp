/*
Ingredient Adjuster
A cookie recipe calls for the following ingredients:
• 1.5 cups of sugar
• 1 cup of butter
• 2.75 cups of flour
The recipe produces 48 cookies with this amount of the ingredients. 
Write a program that asks the user how many cookies he or she wants to make, and 
then displays the number of cups of each ingredient needed for the specified 
number of cookies.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr int ORIGINAL_COOKIES = 48;
    constexpr double ORIGINAL_SUGAR = 1.5;
    constexpr double ORIGINAL_BUTTER = 1.0;
    constexpr double ORIGINAL_FLOUR = 2.75;

    int number_cookies = 0;
    double cups_sugar = 0;
    double cups_butter = 0;
    double cups_flour = 0;
    double recipe_factor = 0;

    cout << "Number of cookies to make: ";
    cin >> number_cookies;

    cout << "The new recipe wiil be:\n";

    // implement rule of three to obtai percentage of original recipe
    recipe_factor = (number_cookies * 100) / ORIGINAL_COOKIES;

    cups_sugar = ORIGINAL_SUGAR * (recipe_factor / 100.0);
    cups_butter = ORIGINAL_BUTTER * (recipe_factor / 100.0);
    cups_flour = ORIGINAL_FLOUR * (recipe_factor / 100.0);

    cout << setprecision(2) << fixed;
    cout << cups_sugar << " cups of sugar\n";
    cout << cups_butter << " cups of butter\n";
    cout << cups_flour << " cups of flour\n";
}