/*
Stadium Seating
There are three seating categories at a stadium. For a softball game, Class A 
seats cost $15, Class B seats cost $12, and Class C seats cost $9. Write a 
program that asks how many tickets for each class of seats were sold, then 
displays the amount of income generated from ticket sales. Format your dollar 
amount in fixed-point notation, with two decimal places of precision, and be 
sure the decimal point is always displayed.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr int CLASS_A_COST = 15;
    constexpr int CLASS_B_COST = 12;
    constexpr int CLASS_C_COST =  9;

    int ticketsA = 0;
    int ticketsB = 0;
    int ticketsC = 0;

    double amount_income = 0;

    cout << "How many Class A seats? ";
    cin >> ticketsA;

    cout << "How many Class B seats? ";
    cin >> ticketsB;
    
    cout << "How many Class C seats? ";
    cin >> ticketsC;

    cout << "\n\nThe amount of ticket sales is:\n";

    amount_income = (ticketsA * CLASS_A_COST) + (ticketsB * CLASS_B_COST) +
                    (ticketsC * CLASS_C_COST);

    cout << setprecision(2) << fixed;
    cout << " $" << amount_income << '\n';

    return 0;
}