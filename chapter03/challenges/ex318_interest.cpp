/*
Interest Earned
Assuming there are no deposits other than the original investment, the balance 
in a savings account after one year may be calculated as

Amount = Principal * (1 + Rate/T)^T

Principal is the balance in the savings account, Rate is the interest rate, and 
T is the number of times the interest is compounded during a year (T is 4 if the 
interest is compounded quarterly).
Write a program that asks for the principal, the interest rate, and the number 
of times the interest is compounded. It should display a report similar to

Interest Rate:                      4.25%
Times Compounded:                     12 
Principal:                     $ 1000.00
Interest:                      $   43.34
Amount in Savings:             $ 1043.34

*/

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main()
{
    double principal = 0;
    double amount_savings = 0;
    double rate = 0;
    double interest = 0;
    double t = 0;              // times compunded

    cout << "Enter principal amount: ";
    cin >> principal;
    cout << "Enter interest rate: ";
    cin >> rate;
    cout << "Enter # of compound per year: ";
    cin >> t;
    
    cout << "\n\nREPORT====================\n";

    rate /= 100.0;
    amount_savings = principal * (pow((1 + rate/t),t));
    interest = amount_savings  - principal;
    cout << setprecision(2) << fixed;

    cout << "Interest Rate:     \t " << setw(9) << rate * 100 << "%" << '\n';
    cout << "Times compunded:   \t " << setw(9) << (int) t << '\n';
    cout << "Principal:         \t$" << setw(9) << principal << '\n';
    cout << "Interest:          \t$" << setw(9) << interest << '\n';
    cout << "Amount in savings: \t$" << setw(9) << amount_savings << endl;

    return 0;
}