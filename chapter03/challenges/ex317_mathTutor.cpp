/*
Math Tutor
Write a program that can be used as a math tutor for a young student. 
The program should display two random numbers to be added, such as

 247
+129

The program should then pause while the student works on the problem. When the
student is ready to check the answer, he or she can press a key and the program 
will display the correct solution: 

 247
+129
376
*/

#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{   
    constexpr int MIN_VALUE = 100;
    constexpr int MAX_VALUE = 999;

    int value1 = 0;
    int value2 = 0;
    int answer = 0;

    cout << "Answer the exercise. When you finish type ENTER:\n";
    // Use srand directly with time(0)
    srand(time(0));

    // or use a variable to hold the value
    // unsigned seed = time(0);
    // srand(seed);

    // using rand directly generate big random numbers
    // value1 = rand();
    // value2 = rand();

    // to limit from 100 to 999
    value1 = (rand() % (MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    value2 = (rand() % (MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    answer = value1 + value2;

    cout << "  " << value1 << '\n';
    cout << "+ " << value2 << '\n';

    // wait for student to press a key
    cin.get();

    // display complete result
    cout << "  " << value1 << '\n';
    cout << "+ " << value2 << '\n';
    cout << "----\n";
    cout << "  " << answer << endl;

    return 0;
}