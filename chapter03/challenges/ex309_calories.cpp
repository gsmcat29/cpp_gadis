/*
How Many Calories?
A bag of cookies holds 30 cookies. The calorie information on the bag claims 
that there are 10 “servings” in the bag and that a serving equals 300 calories. 
Write a program that asks the user to input how many cookies he or she actually 
ate and then reports how many total calories were consumed.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int bag_cookies = 30;
    int bag_servings = 10;
    int bag_calories = 300;

    int number_cookies = 0;
    double total_calories = 0;
    // ask user for input
    cout << "How many cookies did you eat? ";
    cin >> number_cookies;


    cout << "The total number of calories consumed: ";
    // implement rule of three for the servings
    total_calories = (number_cookies * bag_servings) / static_cast<double> (bag_cookies);
    total_calories *= bag_calories;

    cout << setprecision(2) << fixed << total_calories << " calories";

    cout << endl;

    return 0;
}