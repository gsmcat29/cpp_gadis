/*
How Much Insurance?
Many financial experts advise that property owners should insure their homes or 
buildings for at least 80 percent of the amount it would cost to replace the 
structure. Write a program that asks the user to enter the replacement cost of a 
building and then displays the minimum amount of insurance he or she should buy 
for the property.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    constexpr double PROPERTY_TAX = 0.80;
    double replacement_cost = 0;
    double min_amount = 0;

    cout << "Enter property replacement cost: ";
    cin >> replacement_cost;

    min_amount = replacement_cost * PROPERTY_TAX;

    cout << "The minimum amount of insurance : $";
    cout << setprecision(2) << fixed;
    cout << min_amount << endl;

    return 0;
}